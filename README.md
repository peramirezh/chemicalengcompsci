# Introduction

This page aim to present multiple computational tools that can be used by chemical engineers to solve problems related to modeling, simulation, optimization and design of process lines, operation units and mass fennomena.


# Software and tools used

- [Aspen+](https://www.aspentech.com/en/products/engineering/aspen-plus)
- [Comsol](https://www.comsol.com/)
- [Python](https://www.python.org/)

# Getting started

First thing to do is update `main.go` with your new project path:

```diff
-       proto "gitlab.com/gitlab-org/project-templates/go-micro/proto"
+       proto "gitlab.com/$YOUR_NAMESPACE/$PROJECT_NAME/proto"
```

Note that these are not actual environment variables, but values you should
replace.

## What's contained in this project

- software folders - are all the folders with the content
- examples - examples folders

## Guides

Guides to install the tools used 

- [Aspen+](null)
- [Comsol](null)
- [Python](null)
