'
' spry.vba
'

' Model exported on Aug 15 2020, 02:35 by COMSOL 5.4.0.388.
Sub run()
  Dim modelutil As ComsolCom.ModelUtil
  Set modelutil = CreateObject("ComsolCom.ModelUtil")
  Dim model As ModelImpl
  Set model = modelutil.Create("Model")

  Call model.modelPath("C:\Users\pcsim2\Documents\peramirezh\COMSOL\examples")

  Call model.component().create("comp1", True)

  Call model.get_component("comp1").geom().create("geom1", 3)

  Call model.get_component("comp1").mesh().create("mesh1")

  Call model.param().set("u_inlet", "1[m/s]")
  Call model.param().set("r_inlet", "5[cm]")
  Call model.param().set("r_outlet", "5[cm]")
  Call model.param().set("R_f", "0.05")
  Call model.param().set("u_outlet", "R_f*2*(r_inlet/r_outlet)^2*u_inlet")
  Call model.param().set("u_inlet", "5[m/s]")

  Call model.get_component("comp1").get_geom("geom1").feature().create("wp1", "WorkPlane")
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").set("unite", True)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().create("pol1", "Polygon")
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").set("source", "table")
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 0, 0, 0)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", "r_outlet", 0, 0)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 0, 0, 1)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 0.4, 1, 0)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 0.2, 1, 1)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_run("pol1")
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 0.3, 2, 0)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 1, 2, 1)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 0.2, 2, 0)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_run("pol1")
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 0.4, 2, 0)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_run("pol1")
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 0.35, 3, 0)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 1, 3, 1)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_run("pol1")
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_run("pol1")
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 0.3, 3, 0)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_run("pol1")
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 0.3, 4, 0)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 1.2, 4, 1)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_run("pol1")
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 0, 5, 0)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 1.2, 5, 1)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_run("pol1")
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 0, 6, 0)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 0, 6, 1)

  Dim temp0(0 to 0) as Long
  temp0(0) = 6

  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").move("table", temp0, -1)

  Dim temp1(0 to 0) as Long
  temp1(0) = 6

  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").move("tableconstr", temp1, -1)

  Dim temp2(0 to 0) as Long
  temp2(0) = 5

  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").move("table", temp2, -1)

  Dim temp3(0 to 0) as Long
  temp3(0) = 5

  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").move("tableconstr", temp3, -1)

  Dim temp4(0 to 0) as Long
  temp4(0) = 4

  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").move("table", temp4, -1)

  Dim temp5(0 to 0) as Long
  temp5(0) = 4

  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").move("tableconstr", temp5, -1)

  Dim temp6(0 to 0) as Long
  temp6(0) = 3

  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").move("table", temp6, -1)

  Dim temp7(0 to 0) as Long
  temp7(0) = 3

  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").move("tableconstr", temp7, -1)

  Dim temp8(0 to 0) as Long
  temp8(0) = 2

  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").move("table", temp8, -1)

  Dim temp9(0 to 0) as Long
  temp9(0) = 2

  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").move("tableconstr", temp9, -1)

  Dim temp10(0 to 0) as Long
  temp10(0) = 1

  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").move("table", temp10, -1)

  Dim temp11(0 to 0) as Long
  temp11(0) = 1

  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").move("tableconstr", temp11, -1)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_run("pol1")
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 0.5, 2, 1)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_run("pol1")
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 1.1, 5, 1)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 1.1, 6, 1)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_run("pol1")
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 0.2, 4, 0)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 0.2, 5, 0)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_run("pol1")
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", "r_inlet", 1, 0)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_run("pol1")
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_run("pol1")
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", "r_ounlet", 1, 0)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", "r_oulet", 1, 0)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", "r_outlet", 1, 0)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_run("pol1")
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_run("pol1")
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_run("pol1")
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 1.2, 5, 1)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 1.2, 6, 1)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_run("pol1")
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 1.5, 3, 1)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 1.5, 4, 1)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 1.7, 5, 1)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 1.7, 6, 1)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_run("pol1")
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 0.5, 2, 0)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 0.5, 3, 0)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_run("pol1")
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 0.6, 2, 0)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 0.6, 3, 0)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 0.4, 4, 0)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 0.4, 5, 0)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_run("pol1")
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 1, 3, 1)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 1, 4, 1)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_run("pol1")
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 1.15, 5, 1)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 1.15, 6, 1)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_run("pol1")
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 0.5, 2, 0)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 0.5, 3, 0)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_run("pol1")
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 0.6, 2, 1)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_run("pol1")
  Call model.get_component("comp1").get_geom("geom1").get_run("wp1")
  Call model.get_component("comp1").get_geom("geom1").feature().create("rev1", "Revolve")
  Call model.get_component("comp1").get_geom("geom1").get_feature("rev1").set("workplane", "wp1")

  Dim temp12(0 to 0) as String
  temp12(0) = "wp1"

  Call model.get_component("comp1").get_geom("geom1").get_feature("rev1").get_selection("input").set(temp12)
  Call model.get_component("comp1").get_geom("geom1").get_feature("rev1").set("angtype", "full")
  Call model.get_component("comp1").get_geom("geom1").get_run("rev1")
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").set("quickplane", "zx")
  Call model.get_component("comp1").get_geom("geom1").runPre("fin")
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").set("quickplane", "yx")
  Call model.get_component("comp1").get_geom("geom1").runPre("fin")
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").set("quickplane", "yz")
  Call model.get_component("comp1").get_geom("geom1").get_run("wp1")
  Call model.get_component("comp1").get_geom("geom1").runPre("fin")
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").set("quickplane", "xz")
  Call model.get_component("comp1").get_geom("geom1").runPre("fin")
  Call model.get_component("comp1").get_geom("geom1").get_run("rev1")
  Call model.get_component("comp1").get_geom("geom1").create("cyl1", "Cylinder")
  Call model.get_component("comp1").get_geom("geom1").get_feature("cyl1").set("r", "r_inlet")
  Call model.get_component("comp1").get_geom("geom1").get_feature("cyl1").set("h", 0.2)
  Call model.get_component("comp1").get_geom("geom1").get_run("cyl1")

  Call model.get_component("comp1").get_view("view1").set("transparency", True)

  Call model.get_component("comp1").get_geom("geom1").get_feature("cyl1").set("axistype", "y")
  Call model.get_component("comp1").get_geom("geom1").get_run("cyl1")
  Call model.get_component("comp1").get_geom("geom1").runPre("fin")

  Dim temp13(0 to 2) as Long
  temp13(0) = 0
  temp13(1) = 0
  temp13(2) = 1

  Call model.get_component("comp1").get_geom("geom1").get_feature("cyl1").set("pos", temp13)
  Call model.get_component("comp1").get_geom("geom1").runPre("fin")

  Call model.get_component("comp1").get_view("view1").camera().set("projection", "orthographic")
  Call model.get_component("comp1").get_view("view1").set("transparency", False)
  Call model.get_component("comp1").get_view("view1").set("scenelight", False)
  Call model.get_component("comp1").get_view("view1").set("transparency", True)
  Call model.get_component("comp1").get_view("view1").set("renderwireframe", False)

  Dim temp14(0 to 2) as Double
  temp14(0) = 0
  temp14(1) = 0
  temp14(2) = 1.1

  Call model.get_component("comp1").get_geom("geom1").get_feature("cyl1").set("pos", temp14)
  Call model.get_component("comp1").get_geom("geom1").runPre("fin")

  Dim temp15(0 to 2) as String
  temp15(0) = "0"
  temp15(1) = "0"
  temp15(2) = "1.0.5"

  Call model.get_component("comp1").get_geom("geom1").get_feature("cyl1").set("pos", temp15)
  Call model.get_component("comp1").get_geom("geom1").get_run("rev1")

  Dim temp16(0 to 2) as Double
  temp16(0) = 0
  temp16(1) = 0
  temp16(2) = 1.05

  Call model.get_component("comp1").get_geom("geom1").get_feature("cyl1").set("pos", temp16)
  Call model.get_component("comp1").get_geom("geom1").runPre("fin")

  Dim temp17(0 to 2) as Double
  temp17(0) = 0
  temp17(1) = 0
  temp17(2) = 1.075

  Call model.get_component("comp1").get_geom("geom1").get_feature("cyl1").set("pos", temp17)
  Call model.get_component("comp1").get_geom("geom1").runPre("fin")
  Call model.get_component("comp1").get_geom("geom1").get_feature("cyl1").set("h", 0.3)
  Call model.get_component("comp1").get_geom("geom1").runPre("fin")

  Dim temp18(0 to 2) as Double
  temp18(0) = 0
  temp18(1) = -0.2
  temp18(2) = 1.075

  Call model.get_component("comp1").get_geom("geom1").get_feature("cyl1").set("pos", temp18)
  Call model.get_component("comp1").get_geom("geom1").runPre("fin")

  Dim temp19(0 to 2) as Double
  temp19(0) = 0
  temp19(1) = -1
  temp19(2) = 1.075

  Call model.get_component("comp1").get_geom("geom1").get_feature("cyl1").set("pos", temp19)
  Call model.get_component("comp1").get_geom("geom1").runPre("fin")

  Dim temp20(0 to 2) as Double
  temp20(0) = 0
  temp20(1) = -0.7
  temp20(2) = 1.075

  Call model.get_component("comp1").get_geom("geom1").get_feature("cyl1").set("pos", temp20)
  Call model.get_component("comp1").get_geom("geom1").runPre("fin")

  Dim temp21(0 to 2) as Double
  temp21(0) = 0
  temp21(1) = -0.5
  temp21(2) = 1.075

  Call model.get_component("comp1").get_geom("geom1").get_feature("cyl1").set("pos", temp21)
  Call model.get_component("comp1").get_geom("geom1").runPre("fin")

  Dim temp22(0 to 2) as Double
  temp22(0) = 0
  temp22(1) = -0.6
  temp22(2) = 1.075

  Call model.get_component("comp1").get_geom("geom1").get_feature("cyl1").set("pos", temp22)
  Call model.get_component("comp1").get_geom("geom1").runPre("fin")

  Dim temp23(0 to 2) as Double
  temp23(0) = 0.4
  temp23(1) = -0.6
  temp23(2) = 1.075

  Call model.get_component("comp1").get_geom("geom1").get_feature("cyl1").set("pos", temp23)
  Call model.get_component("comp1").get_geom("geom1").runPre("fin")

  Dim temp24(0 to 2) as Double
  temp24(0) = 0.3
  temp24(1) = -0.6
  temp24(2) = 1.075

  Call model.get_component("comp1").get_geom("geom1").get_feature("cyl1").set("pos", temp24)
  Call model.get_component("comp1").get_geom("geom1").runPre("fin")

  Dim temp25(0 to 2) as Double
  temp25(0) = 0.35
  temp25(1) = -0.6
  temp25(2) = 1.075

  Call model.get_component("comp1").get_geom("geom1").get_feature("cyl1").set("pos", temp25)
  Call model.get_component("comp1").get_geom("geom1").runPre("fin")

  Dim temp26(0 to 2) as Double
  temp26(0) = 0.34
  temp26(1) = -0.6
  temp26(2) = 1.075

  Call model.get_component("comp1").get_geom("geom1").get_feature("cyl1").set("pos", temp26)
  Call model.get_component("comp1").get_geom("geom1").runPre("fin")

  Dim temp27(0 to 2) as Double
  temp27(0) = 0.34
  temp27(1) = -0.2
  temp27(2) = 1.075

  Call model.get_component("comp1").get_geom("geom1").get_feature("cyl1").set("pos", temp27)
  Call model.get_component("comp1").get_geom("geom1").runPre("fin")

  Dim temp28(0 to 2) as Double
  temp28(0) = 0.34
  temp28(1) = -0.3
  temp28(2) = 1.075

  Call model.get_component("comp1").get_geom("geom1").get_feature("cyl1").set("pos", temp28)
  Call model.get_component("comp1").get_geom("geom1").runPre("fin")

  Dim temp29(0 to 2) as Double
  temp29(0) = 0.34
  temp29(1) = -0.4
  temp29(2) = 1.075

  Call model.get_component("comp1").get_geom("geom1").get_feature("cyl1").set("pos", temp29)
  Call model.get_component("comp1").get_geom("geom1").runPre("fin")

  Dim temp30(0 to 2) as Double
  temp30(0) = 0.34
  temp30(1) = -0.35
  temp30(2) = 1.075

  Call model.get_component("comp1").get_geom("geom1").get_feature("cyl1").set("pos", temp30)
  Call model.get_component("comp1").get_geom("geom1").runPre("fin")

  Call model.get_component("comp1").get_view("view1").set("transparency", False)

  Call model.get_component("comp1").get_geom("geom1").get_run("fin")

  Call model.get_component("comp1").get_view("view1").set("transparency", False)

  Call model.get_component("comp1").get_geom("geom1").get_run("cyl1")
  Call model.get_component("comp1").get_geom("geom1").create("co1", "Compose")

  Dim temp31(0 to 1) as String
  temp31(0) = "cyl1"
  temp31(1) = "rev1"

  Call model.get_component("comp1").get_geom("geom1").get_feature("co1").get_selection("input").set(temp31)
  Call model.get_component("comp1").get_geom("geom1").feature().remove("co1")
  Call model.get_component("comp1").get_geom("geom1").get_run("fin")
  Call model.get_component("comp1").get_geom("geom1").create("cmd1", "CompositeDomains")

  Dim temp32(0 to 1) as Long
  temp32(0) = 1
  temp32(1) = 2

  Call model.get_component("comp1").get_geom("geom1").get_feature("cmd1").get_selection("input").set("fin", temp32)
  Call model.get_component("comp1").get_geom("geom1").get_run("cmd1")

  Call model.get_component("comp1").get_view("view1").set("transparency", True)

  Call model.get_component("comp1").get_geom("geom1").runPre("cmd1")

  Call model.get_component("comp1").get_view("view1").set("hidestatus", "ignore")

  Dim temp33(0 to 1) as Long
  temp33(0) = 1
  temp33(1) = 2

  Call model.get_component("comp1").get_geom("geom1").get_feature("cmd1").get_selection("input").set("fin", temp33)

  Call model.get_component("comp1").get_view("view1").set("hidestatus", "hide")
  Call model.get_component("comp1").get_view("view1").hideObjects().create("hide1")
  Call model.get_component("comp1").get_view("view1").get_hideObjects("hide1").init(3)

  Dim temp34(0 to 0) as Long
  temp34(0) = 1

  Call model.get_component("comp1").get_view("view1").get_hideObjects("hide1").add("fin", temp34)

  Dim temp35(0 to 2) as Long
  temp35(0) = 1
  temp35(1) = 2
  temp35(2) = 3

  Call model.get_component("comp1").get_geom("geom1").get_feature("cmd1").get_selection("input").set("fin", temp35)
  Call model.get_component("comp1").get_geom("geom1").get_run("cmd1")

  Call model.get_component("comp1").get_view("view1").hideObjects().clear()

  Call model.get_component("comp1").material().create("mat1", "Common")

  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").func().create("eta", "Piecewise")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").func().create("Cp", "Piecewise")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").func().create("rho", "Analytic")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").func().create("k", "Piecewise")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").func().create("cs", "Analytic")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").func().create("an1", "Analytic")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").func().create("an2", "Analytic")
  Call model.get_component("comp1").get_material("mat1").propertyGroup().create("NonlinearModel", "Nonlinear model")
  Call model.get_component("comp1").get_material("mat1").label("Air")
  Call model.get_component("comp1").get_material("mat1").set("family", "air")

  Dim temp36(0 to 0, 0 to 1) as String
  temp36(0, 0) = "gases"
  temp36(0, 1) = "Gases"

  Call model.get_component("comp1").get_material("mat1").set("groups", temp36)
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("eta").set("arg", "T")

  Dim temp37(0 to 0, 0 to 2) as String
  temp37(0, 0) = "200.0"
  temp37(0, 1) = "1600.0"
  temp37(0, 2) = "-8.38278E-7+8.35717342E-8*T^1-7.69429583E-11*T^2+4.6437266E-14*T^3-1.06585607E-17*T^4"

  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("eta").set("pieces", temp37)
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("eta").set("argunit", "K")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("eta").set("fununit", "Pa*s")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("Cp").set("arg", "T")

  Dim temp38(0 to 0, 0 to 2) as String
  temp38(0, 0) = "200.0"
  temp38(0, 1) = "1600.0"
  temp38(0, 2) = "1047.63657-0.372589265*T^1+9.45304214E-4*T^2-6.02409443E-7*T^3+1.2858961E-10*T^4"

  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("Cp").set("pieces", temp38)
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("Cp").set("argunit", "K")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("Cp").set("fununit", "J/(kg*K)")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("rho").set("expr", "pA*0.02897/R_const[K*mol/J]/T")

  Dim temp39(0 to 1) as String
  temp39(0) = "pA"
  temp39(1) = "T"

  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("rho").set("args", temp39)
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("rho").set("dermethod", "manual")

  Dim temp40(0 to 1, 0 to 1) as String
  temp40(0, 0) = "pA"
  temp40(0, 1) = "d(pA*0.02897/R_const/T,pA)"
  temp40(1, 0) = "T"
  temp40(1, 1) = "d(pA*0.02897/R_const/T,T)"

  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("rho").set("argders", temp40)
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("rho").set("argunit", "Pa,K")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("rho").set("fununit", "kg/m^3")

  Dim temp41(0 to 1, 0 to 2) as String
  temp41(0, 0) = "pA"
  temp41(0, 1) = "0"
  temp41(0, 2) = "1"
  temp41(1, 0) = "T"
  temp41(1, 1) = "0"
  temp41(1, 2) = "1"

  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("rho").set("plotargs", temp41)
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("k").set("arg", "T")

  Dim temp42(0 to 0, 0 to 2) as String
  temp42(0, 0) = "200.0"
  temp42(0, 1) = "1600.0"
  temp42(0, 2) = "-0.00227583562+1.15480022E-4*T^1-7.90252856E-8*T^2+4.11702505E-11*T^3-7.43864331E-15*T^4"

  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("k").set("pieces", temp42)
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("k").set("argunit", "K")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("k").set("fununit", "W/(m*K)")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("cs").set("expr", "sqrt(1.4*R_const[K*mol/J]/0.02897*T)")

  Dim temp43(0 to 0) as String
  temp43(0) = "T"

  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("cs").set("args", temp43)
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("cs").set("dermethod", "manual")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("cs").set("argunit", "K")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("cs").set("fununit", "m/s")

  Dim temp44(0 to 0, 0 to 2) as String
  temp44(0, 0) = "T"
  temp44(0, 1) = "273.15"
  temp44(0, 2) = "373.15"

  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("cs").set("plotargs", temp44)
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("an1").label("Analytic ")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("an1").set("funcname", "alpha_p")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("an1").set("expr", "-1/rho(pA,T)*d(rho(pA,T),T)")

  Dim temp45(0 to 1) as String
  temp45(0) = "pA"
  temp45(1) = "T"

  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("an1").set("args", temp45)
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("an1").set("argunit", "Pa, K")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("an1").set("fununit", "1/K")

  Dim temp46(0 to 1, 0 to 2) as String
  temp46(0, 0) = "pA"
  temp46(0, 1) = "101325"
  temp46(0, 2) = "101325"
  temp46(1, 0) = "T"
  temp46(1, 1) = "273.15"
  temp46(1, 2) = "373.15"

  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("an1").set("plotargs", temp46)
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("an2").set("funcname", "muB")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("an2").set("expr", "0.6*eta(T)")

  Dim temp47(0 to 0) as String
  temp47(0) = "T"

  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("an2").set("args", temp47)
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("an2").set("argunit", "K")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("an2").set("fununit", "Pa*s")

  Dim temp48(0 to 0, 0 to 2) as String
  temp48(0, 0) = "T"
  temp48(0, 1) = "200"
  temp48(0, 2) = "1600"

  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("an2").set("plotargs", temp48)
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").set("thermalexpansioncoefficient", "")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").set("molarmass", "")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").set("bulkviscosity", "")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").set("dynamicviscosity", "eta(T)")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").set("ratioofspecificheat", "1.4")

  Dim temp49(0 to 8) as String
  temp49(0) = "0[S/m]"
  temp49(1) = "0"
  temp49(2) = "0"
  temp49(3) = "0"
  temp49(4) = "0[S/m]"
  temp49(5) = "0"
  temp49(6) = "0"
  temp49(7) = "0"
  temp49(8) = "0[S/m]"

  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").set("electricconductivity", temp49)
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").set("heatcapacity", "Cp(T)")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").set("density", "rho(pA,T)")

  Dim temp50(0 to 8) as String
  temp50(0) = "k(T)"
  temp50(1) = "0"
  temp50(2) = "0"
  temp50(3) = "0"
  temp50(4) = "k(T)"
  temp50(5) = "0"
  temp50(6) = "0"
  temp50(7) = "0"
  temp50(8) = "k(T)"

  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").set("thermalconductivity", temp50)
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").set("soundspeed", "cs(T)")

  Dim temp51(0 to 8) as String
  temp51(0) = "alpha_p(pA,T)"
  temp51(1) = "0"
  temp51(2) = "0"
  temp51(3) = "0"
  temp51(4) = "alpha_p(pA,T)"
  temp51(5) = "0"
  temp51(6) = "0"
  temp51(7) = "0"
  temp51(8) = "alpha_p(pA,T)"

  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").set("thermalexpansioncoefficient", temp51)
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").set("molarmass", "0.02897")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").set("bulkviscosity", "muB(T)")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").addInput("temperature")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").addInput("pressure")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("NonlinearModel").set("BA", "(def.gamma+1)/2")
  Call model.get_component("comp1").get_material("mat1").materialType("nonSolid")
  Call model.get_component("comp1").get_material("mat1").set("family", "air")

  Call model.get_component("comp1").get_mesh("mesh1").run()
  Call model.get_component("comp1").get_mesh("mesh1").autoMeshSize(3)
  Call model.get_component("comp1").get_mesh("mesh1").run()

  Call model.get_component("comp1").physics().create("spf", "TurbulentFlowkeps", "geom1")

  Call model.get_component("comp1").get_mesh("mesh1").autoMeshSize(2)
  Call model.get_component("comp1").get_mesh("mesh1").run()
  Call model.get_component("comp1").get_mesh("mesh1").autoMeshSize(3)
  Call model.get_component("comp1").get_mesh("mesh1").run()
  Call model.get_component("comp1").get_mesh("mesh1").autoMeshSize(4)
  Call model.get_component("comp1").get_mesh("mesh1").run()
  Call model.get_component("comp1").get_mesh("mesh1").autoMeshSize(5)
  Call model.get_component("comp1").get_mesh("mesh1").run()

  Call model.get_component("comp1").get_physics("spf").create("inl1", "InletBoundary", 2)

  Dim temp52(0 to 0) as Long
  temp52(0) = 28

  Call model.get_component("comp1").get_physics("spf").get_feature("inl1").selection().set(temp52)
  Call model.get_component("comp1").get_physics("spf").create("out1", "OutletBoundary", 2)

  Dim temp53(0 to 3) as Long
  temp53(0) = 11
  temp53(1) = 12
  temp53(2) = 18
  temp53(3) = 20

  Call model.get_component("comp1").get_physics("spf").get_feature("out1").selection().set(temp53)

  Call model.get_component("comp1").get_geom("geom1").get_feature("rev1").set("origfaces", False)
  Call model.get_component("comp1").get_geom("geom1").runPre("fin")
  Call model.get_component("comp1").get_geom("geom1").get_run("cmd1")
  Call model.get_component("comp1").get_geom("geom1").get_run("cmd1")
  Call model.get_component("comp1").get_geom("geom1").create("cmf1", "CompositeFaces")

  Dim temp54(0 to 3) as Long
  temp54(0) = 11
  temp54(1) = 12
  temp54(2) = 18
  temp54(3) = 19

  Call model.get_component("comp1").get_geom("geom1").get_feature("cmf1").get_selection("input").set("cmd1", temp54)
  Call model.get_component("comp1").get_geom("geom1").get_run("cmf1")
  Call model.get_component("comp1").get_geom("geom1").feature().move("cmf1", 4)
  Call model.get_component("comp1").get_geom("geom1").get_run("cmd1")

  Call model.get_component("comp1").get_mesh("mesh1").run()

  Call model.study().create("std1")

  Call model.get_study("std1").setGenConv(True)
  Call model.get_study("std1").create("stat", "Stationary")
  Call model.get_study("std1").get_feature("stat").activate("spf", True)

  Call model.get_component("comp1").get_physics("spf").get_feature("inl1").set("U0in", "u_inlet")

  Call model.sol().create("sol1")

  Call model.get_sol("sol1").study("std1")

  Call model.get_study("std1").get_feature("stat").set("notlistsolnum", 1)
  Call model.get_study("std1").get_feature("stat").set("notsolnum", "1")
  Call model.get_study("std1").get_feature("stat").set("listsolnum", 1)
  Call model.get_study("std1").get_feature("stat").set("solnum", "1")

  Call model.get_sol("sol1").create("st1", "StudyStep")
  Call model.get_sol("sol1").get_feature("st1").set("study", "std1")
  Call model.get_sol("sol1").get_feature("st1").set("studystep", "stat")
  Call model.get_sol("sol1").create("v1", "Variables")
  Call model.get_sol("sol1").get_feature("v1").set("control", "stat")
  Call model.get_sol("sol1").create("s1", "Stationary")
  Call model.get_sol("sol1").get_feature("s1").get_feature("aDef").set("cachepattern", True)
  Call model.get_sol("sol1").get_feature("s1").create("se1", "Segregated")
  Call model.get_sol("sol1").get_feature("s1").get_feature("se1").feature().remove("ssDef")
  Call model.get_sol("sol1").get_feature("s1").get_feature("se1").create("ss1", "SegregatedStep")

  Dim temp55(0 to 1) as String
  temp55(0) = "comp1_u"
  temp55(1) = "comp1_p"

  Call model.get_sol("sol1").get_feature("s1").get_feature("se1").get_feature("ss1").set("segvar", temp55)
  Call model.get_sol("sol1").get_feature("s1").get_feature("se1").get_feature("ss1").set("subdamp", 0.5)
  Call model.get_sol("sol1").get_feature("s1").create("i1", "Iterative")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").set("linsolver", "gmres")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").set("prefuntype", "left")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").set("itrestart", 50)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").set("rhob", 20)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").set("maxlinit", 200)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").set("nlinnormuse", "on")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").label("Algebraic Multigrid Solver (spf)")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").create("mg1", "Multigrid")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").set("prefun", "saamg")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").set("mgcycle", "f")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").set("maxcoarsedof", 80000)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").set("strconn", 0.02)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").set("usesmooth", False)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").set("saamgcompwise", True)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("pr").create("sc1", "SCGS")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("pr").get_feature("sc1").set("linesweeptype", "ssor")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("pr").get_feature("sc1").set("iter", 0)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("pr").get_feature("sc1").set("scgsrelax", 0.7)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("pr").get_feature("sc1").set("scgsmethod", "lines_vertices")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("pr").get_feature("sc1").set("scgsvertexrelax", 0.7)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("pr").get_feature("sc1").set("seconditer", 1)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("pr").get_feature("sc1").set("relax", 0.5)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("po").create("sc1", "SCGS")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("po").get_feature("sc1").set("linesweeptype", "ssor")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("po").get_feature("sc1").set("iter", 1)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("po").get_feature("sc1").set("scgsrelax", 0.7)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("po").get_feature("sc1").set("scgsmethod", "lines_vertices")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("po").get_feature("sc1").set("scgsvertexrelax", 0.7)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("po").get_feature("sc1").set("seconditer", 1)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("po").get_feature("sc1").set("relax", 0.5)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("cs").create("d1", "Direct")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("cs").get_feature("d1").set("linsolver", "pardiso")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("cs").get_feature("d1").set("pivotperturb", 1.0E-13)
  Call model.get_sol("sol1").get_feature("s1").get_feature("se1").get_feature("ss1").set("linsolver", "i1")
  Call model.get_sol("sol1").get_feature("s1").get_feature("se1").get_feature("ss1").label("Velocity u, Pressure p")
  Call model.get_sol("sol1").get_feature("s1").get_feature("se1").create("ss2", "SegregatedStep")

  Dim temp56(0 to 1) as String
  temp56(0) = "comp1_k"
  temp56(1) = "comp1_ep"

  Call model.get_sol("sol1").get_feature("s1").get_feature("se1").get_feature("ss2").set("segvar", temp56)
  Call model.get_sol("sol1").get_feature("s1").get_feature("se1").get_feature("ss2").set("subdamp", 0.35)
  Call model.get_sol("sol1").get_feature("s1").get_feature("se1").get_feature("ss2").set("subiter", 3)
  Call model.get_sol("sol1").get_feature("s1").get_feature("se1").get_feature("ss2").set("subtermconst", "itertol")
  Call model.get_sol("sol1").get_feature("s1").get_feature("se1").get_feature("ss2").set("subntolfact", 1)
  Call model.get_sol("sol1").get_feature("s1").create("i2", "Iterative")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").set("linsolver", "gmres")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").set("prefuntype", "left")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").set("itrestart", 50)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").set("rhob", 20)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").set("maxlinit", 200)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").set("nlinnormuse", "on")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").label("Algebraic Multigrid Solver")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").create("mg1", "Multigrid")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").set("prefun", "saamg")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").set("mgcycle", "v")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").set("maxcoarsedof", 50000)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").set("strconn", 0.01)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").set("usesmooth", False)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").set("saamgcompwise", True)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("pr").create("sl1", "SORLine")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linesweeptype", "ssor")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linerelax", 0.7)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("pr").get_feature("sl1").set("iter", 0)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linealgorithm", "mesh")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linemethod", "uncoupled")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("pr").get_feature("sl1").set("seconditer", 1)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("pr").get_feature("sl1").set("relax", 0.5)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("po").create("sl1", "SORLine")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("po").get_feature("sl1").set("linesweeptype", "ssor")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("po").get_feature("sl1").set("linerelax", 0.7)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("po").get_feature("sl1").set("iter", 1)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("po").get_feature("sl1").set("linealgorithm", "mesh")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("po").get_feature("sl1").set("linemethod", "uncoupled")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("po").get_feature("sl1").set("seconditer", 1)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("po").get_feature("sl1").set("relax", 0.5)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("cs").create("d1", "Direct")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("cs").get_feature("d1").set("linsolver", "pardiso")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("cs").get_feature("d1").set("pivotperturb", 1.0E-13)
  Call model.get_sol("sol1").get_feature("s1").get_feature("se1").get_feature("ss2").set("linsolver", "i2")
  Call model.get_sol("sol1").get_feature("s1").get_feature("se1").get_feature("ss2").label("Turbulence variables")
  Call model.get_sol("sol1").get_feature("s1").get_feature("se1").set("segstabacc", "segcflcmp")
  Call model.get_sol("sol1").get_feature("s1").get_feature("se1").set("subinitcfl", 3)
  Call model.get_sol("sol1").get_feature("s1").get_feature("se1").set("subkppid", 0.65)
  Call model.get_sol("sol1").get_feature("s1").get_feature("se1").set("subkdpid", 0.05)
  Call model.get_sol("sol1").get_feature("s1").get_feature("se1").set("subkipid", 0.05)
  Call model.get_sol("sol1").get_feature("s1").get_feature("se1").set("subcfltol", 0.1)
  Call model.get_sol("sol1").get_feature("s1").get_feature("se1").set("maxsegiter", 400)
  Call model.get_sol("sol1").get_feature("s1").get_feature("se1").create("ll1", "LowerLimit")
  Call model.get_sol("sol1").get_feature("s1").get_feature("se1").get_feature("ll1").set("lowerlimit", "comp1.k 0 comp1.ep 0 ")
  Call model.get_sol("sol1").get_feature("s1").create("i3", "Iterative")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i3").set("linsolver", "gmres")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i3").set("prefuntype", "left")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i3").set("itrestart", 50)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i3").set("rhob", 20)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i3").set("maxlinit", 200)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i3").set("nlinnormuse", "on")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i3").label("Geometric Multigrid Solver (spf)")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i3").create("mg1", "Multigrid")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i3").get_feature("mg1").set("prefun", "gmg")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i3").get_feature("mg1").set("mcasegen", "any")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i3").get_feature("mg1").set("gmglevels", 1)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i3").get_feature("mg1").get_feature("pr").create("sc1", "SCGS")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i3").get_feature("mg1").get_feature("pr").get_feature("sc1").set("linesweeptype", "ssor")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i3").get_feature("mg1").get_feature("pr").get_feature("sc1").set("iter", 0)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i3").get_feature("mg1").get_feature("pr").get_feature("sc1").set("scgsrelax", 0.7)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i3").get_feature("mg1").get_feature("pr").get_feature("sc1").set("scgsmethod", "lines_vertices")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i3").get_feature("mg1").get_feature("pr").get_feature("sc1").set("scgsvertexrelax", 0.7)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i3").get_feature("mg1").get_feature("pr").get_feature("sc1").set("seconditer", 1)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i3").get_feature("mg1").get_feature("pr").get_feature("sc1").set("relax", 0.5)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i3").get_feature("mg1").get_feature("po").create("sc1", "SCGS")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i3").get_feature("mg1").get_feature("po").get_feature("sc1").set("linesweeptype", "ssor")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i3").get_feature("mg1").get_feature("po").get_feature("sc1").set("iter", 1)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i3").get_feature("mg1").get_feature("po").get_feature("sc1").set("scgsrelax", 0.7)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i3").get_feature("mg1").get_feature("po").get_feature("sc1").set("scgsmethod", "lines_vertices")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i3").get_feature("mg1").get_feature("po").get_feature("sc1").set("scgsvertexrelax", 0.7)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i3").get_feature("mg1").get_feature("po").get_feature("sc1").set("seconditer", 1)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i3").get_feature("mg1").get_feature("po").get_feature("sc1").set("relax", 0.5)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i3").get_feature("mg1").get_feature("cs").create("d1", "Direct")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i3").get_feature("mg1").get_feature("cs").get_feature("d1").set("linsolver", "pardiso")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i3").get_feature("mg1").get_feature("cs").get_feature("d1").set("pivotperturb", 1.0E-13)
  Call model.get_sol("sol1").get_feature("s1").create("i4", "Iterative")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i4").set("linsolver", "gmres")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i4").set("prefuntype", "left")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i4").set("itrestart", 50)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i4").set("rhob", 20)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i4").set("maxlinit", 200)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i4").set("nlinnormuse", "on")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i4").label("Geometric Multigrid Solver")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i4").create("mg1", "Multigrid")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i4").get_feature("mg1").set("prefun", "gmg")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i4").get_feature("mg1").set("mcasegen", "any")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i4").get_feature("mg1").set("gmglevels", 1)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i4").get_feature("mg1").get_feature("pr").create("sl1", "SORLine")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i4").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linesweeptype", "ssor")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i4").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linerelax", 0.7)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i4").get_feature("mg1").get_feature("pr").get_feature("sl1").set("iter", 0)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i4").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linealgorithm", "mesh")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i4").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linemethod", "uncoupled")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i4").get_feature("mg1").get_feature("pr").get_feature("sl1").set("seconditer", 1)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i4").get_feature("mg1").get_feature("pr").get_feature("sl1").set("relax", 0.5)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i4").get_feature("mg1").get_feature("po").create("sl1", "SORLine")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i4").get_feature("mg1").get_feature("po").get_feature("sl1").set("linesweeptype", "ssor")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i4").get_feature("mg1").get_feature("po").get_feature("sl1").set("linerelax", 0.7)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i4").get_feature("mg1").get_feature("po").get_feature("sl1").set("iter", 1)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i4").get_feature("mg1").get_feature("po").get_feature("sl1").set("linealgorithm", "mesh")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i4").get_feature("mg1").get_feature("po").get_feature("sl1").set("linemethod", "uncoupled")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i4").get_feature("mg1").get_feature("po").get_feature("sl1").set("seconditer", 1)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i4").get_feature("mg1").get_feature("po").get_feature("sl1").set("relax", 0.5)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i4").get_feature("mg1").get_feature("cs").create("d1", "Direct")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i4").get_feature("mg1").get_feature("cs").get_feature("d1").set("linsolver", "pardiso")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i4").get_feature("mg1").get_feature("cs").get_feature("d1").set("pivotperturb", 1.0E-13)
  Call model.get_sol("sol1").get_feature("s1").feature().remove("fcDef")
  Call model.get_sol("sol1").attach("std1")

  Call model.result().get_dataset("dset1").set("geom", "geom1")
  Call model.result().create("pg1", "PlotGroup3D")

  Call model.get_result("pg1").label("Velocity (spf)")
  Call model.get_result("pg1").set("frametype", "spatial")
  Call model.get_result("pg1").set("data", "dset1")
  Call model.get_result("pg1").feature().create("slc1", "Slice")
  Call model.get_result("pg1").get_feature("slc1").label("Slice")
  Call model.get_result("pg1").get_feature("slc1").set("smooth", "internal")
  Call model.get_result("pg1").get_feature("slc1").set("data", "parent")

  Call model.result().dataset().create("surf1", "Surface")
  Call model.result().get_dataset("surf1").label("Exterior Walls")
  Call model.result().get_dataset("surf1").set("data", "dset1")
  Call model.result().get_dataset("surf1").selection().geom("geom1", 2)

  Dim temp57(0 to 23) as Long
  temp57(0) = 1
  temp57(1) = 2
  temp57(2) = 3
  temp57(3) = 4
  temp57(4) = 5
  temp57(5) = 6
  temp57(6) = 7
  temp57(7) = 8
  temp57(8) = 9
  temp57(9) = 10
  temp57(10) = 12
  temp57(11) = 13
  temp57(12) = 14
  temp57(13) = 15
  temp57(14) = 16
  temp57(15) = 17
  temp57(16) = 18
  temp57(17) = 19
  temp57(18) = 20
  temp57(19) = 21
  temp57(20) = 22
  temp57(21) = 23
  temp57(22) = 25
  temp57(23) = 26

  Call model.result().get_dataset("surf1").selection().set(temp57)
  Call model.result().get_dataset("surf1").selection().inherit(False)
  Call model.result().create("pg2", "PlotGroup3D")

  Call model.get_result("pg2").label("Pressure (spf)")
  Call model.get_result("pg2").set("frametype", "spatial")
  Call model.get_result("pg2").set("data", "surf1")
  Call model.get_result("pg2").feature().create("surf1", "Surface")
  Call model.get_result("pg2").get_feature("surf1").label("Surface")
  Call model.get_result("pg2").get_feature("surf1").set("expr", "1")
  Call model.get_result("pg2").get_feature("surf1").set("titletype", "none")
  Call model.get_result("pg2").get_feature("surf1").set("coloring", "uniform")
  Call model.get_result("pg2").get_feature("surf1").set("color", "gray")
  Call model.get_result("pg2").get_feature("surf1").set("smooth", "internal")
  Call model.get_result("pg2").get_feature("surf1").set("data", "parent")
  Call model.get_result("pg2").feature().create("con1", "Contour")
  Call model.get_result("pg2").get_feature("con1").label("Pressure")
  Call model.get_result("pg2").get_feature("con1").set("expr", "p")
  Call model.get_result("pg2").get_feature("con1").set("number", 40)
  Call model.get_result("pg2").get_feature("con1").set("smooth", "internal")
  Call model.get_result("pg2").get_feature("con1").set("data", "parent")

  Call model.result().create("pg3", "PlotGroup3D")

  Call model.get_result("pg3").label("Wall Resolution (spf)")
  Call model.get_result("pg3").set("frametype", "spatial")
  Call model.get_result("pg3").set("data", "surf1")
  Call model.get_result("pg3").feature().create("surf1", "Surface")
  Call model.get_result("pg3").get_feature("surf1").label("Wall Resolution")
  Call model.get_result("pg3").get_feature("surf1").set("expr", "spf.Delta_wPlus")
  Call model.get_result("pg3").get_feature("surf1").set("smooth", "internal")
  Call model.get_result("pg3").get_feature("surf1").set("data", "parent")

  Call model.get_sol("sol1").runAll()

  Call model.get_result("pg1").run()
  Call model.get_result("pg2").set("data", "surf1")
  Call model.get_result("pg3").set("data", "surf1")
  Call model.get_result("pg2").run()
  Call model.get_result("pg3").run()

  Call model.result().create("pg4", "PlotGroup3D")

  Call model.get_result("pg4").run()
  Call model.get_result("pg4").create("str1", "Streamline")
  Call model.get_result("pg4").get_feature("str1").set("selnumber", 2)

  Dim temp58(0 to 0) as Long
  temp58(0) = 24

  Call model.get_result("pg4").get_feature("str1").selection().set(temp58)
  Call model.get_result("pg4").run()
  Call model.get_result("pg4").get_feature("str1").create("col1", "Color")
  Call model.get_result("pg4").run()
  Call model.get_result("pg4").run()
  Call model.get_result("pg4").run()
  Call model.get_result("pg4").run()
  Call model.get_result("pg4").get_feature("str1").set("linetype", "tube")
  Call model.get_result("pg4").run()
  Call model.get_result("pg4").get_feature("str1").set("radiusexpr", "0.5")
  Call model.get_result("pg4").run()
  Call model.get_result("pg4").set("allowtableupdate", False)
  Call model.get_result("pg4").get_feature("str1").set("tuberadiusscale", 0.009200000000000003)
  Call model.get_result("pg4").get_feature("str1").set("tuberadiusscaleactive", False)
  Call model.get_result("pg4").get_feature("str1").get_feature("col1").set("rangeunit", "m/s")
  Call model.get_result("pg4").get_feature("str1").get_feature("col1").set("rangecolormin", 0.6741774459124247)
  Call model.get_result("pg4").get_feature("str1").get_feature("col1").set("rangecolormax", 5.93946155804264)
  Call model.get_result("pg4").get_feature("str1").get_feature("col1").set("rangecoloractive", "off")

  Dim temp59(0 to 1) as Double
  temp59(0) = 0.6741774459124247
  temp59(1) = 5.93946155804264

  Call model.get_result("pg4").get_feature("str1").get_feature("col1").set("rangeactualminmax", temp59)
  Call model.get_result("pg4").get_feature("str1").get_feature("col1").set("rangeisshared", True)
  Call model.get_result("pg4").get_feature("str1").get_feature("col1").set("rangedatamin", 0.6741774459124247)
  Call model.get_result("pg4").get_feature("str1").get_feature("col1").set("rangedatamax", 5.93946155804264)
  Call model.get_result("pg4").get_feature("str1").get_feature("col1").set("rangedataactive", "off")
  Call model.get_result("pg4").set("allowtableupdate", True)
  Call model.get_result("pg1").run()

  Call model.get_component("comp1").get_view("view1").set("transparency", False)

  Call model.get_result("pg1").run()
  Call model.get_result("pg1").get_feature("slc1").set("quickxnumber", 3)
  Call model.get_result("pg1").run()
  Call model.get_result("pg1").get_feature("slc1").set("quickplane", "xy")
  Call model.get_result("pg1").run()
  Call model.get_result("pg1").get_feature("slc1").set("quickplane", "zx")
  Call model.get_result("pg1").run()

  Call model.label("spry.mph")

  Call model.get_result("pg1").run()
  Call model.get_result("pg2").run()
  Call model.get_result("pg4").run()

End Sub
