'
' hydrocyclone.vba
'

' Model exported on Aug 15 2020, 02:34 by COMSOL 5.4.0.388.
Sub run()
  Dim modelutil As ComsolCom.ModelUtil
  Set modelutil = CreateObject("ComsolCom.ModelUtil")
  Dim model As ModelImpl
  Set model = modelutil.Create("Model")

  Call model.modelPath("C:\Users\pcsim2\Documents\peramirezh\COMSOL\examples")

  Call model.component().create("comp1", True)

  Call model.get_component("comp1").geom().create("geom1", 3)

  Call model.get_component("comp1").mesh().create("mesh1")

  Call model.get_component("comp1").physics().create("spf", "TurbulentFlowv2f", "geom1")

  Call model.study().create("std1")

  Call model.get_study("std1").setGenConv(True)
  Call model.get_study("std1").create("wdi", "WallDistanceInitialization")
  Call model.get_study("std1").get_feature("wdi").set("solnum", "auto")
  Call model.get_study("std1").get_feature("wdi").set("notsolnum", "auto")
  Call model.get_study("std1").get_feature("wdi").set("ngen", "5")
  Call model.get_study("std1").get_feature("wdi").activate("spf", True)
  Call model.get_study("std1").create("stat", "Stationary")
  Call model.get_study("std1").get_feature("stat").set("solnum", "auto")
  Call model.get_study("std1").get_feature("stat").set("notsolnum", "auto")
  Call model.get_study("std1").get_feature("stat").set("ngen", "5")
  Call model.get_study("std1").get_feature("stat").activate("spf", True)

  Call model.param().set("u_in", "5[m/s]")
  Call model.param().descr("u_in", "Inlet velocity")
  Call model.param().set("r_in", "0.0725[m]")
  Call model.param().descr("r_in", "Inlet radius")
  Call model.param().set("r_out", "0.07[m]")
  Call model.param().descr("r_out", "Reject radius")
  Call model.param().set("R_f", "0.05")
  Call model.param().descr("R_f", "Reject volume fraction")
  Call model.param().set("u_out", "R_f*2*(r_in/r_out)^2*u_in")
  Call model.param().descr("u_out", "Reject velocity")

  Call model.get_component("comp1").get_geom("geom1").feature().create("wp1", "WorkPlane")
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").set("unite", True)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").set("quickplane", "xz")
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().create("pol1", "Polygon")
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").set("source", "table")
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 0.4, 0, 0)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 1, 0, 1)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 0.4, 1, 0)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 0.8, 1, 1)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", "r_out", 2, 0)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", -1, 2, 1)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 0, 3, 0)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", -1, 3, 1)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 0, 4, 0)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 1.2, 4, 1)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 0.15, 5, 0)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 1.2, 5, 1)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 0.15, 6, 0)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 0.7, 6, 1)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 0.18, 7, 0)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 0.7, 7, 1)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 0.25, 8, 0)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 1, 8, 1)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 0.4, 9, 0)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_feature("pol1").setIndex("table", 1, 9, 1)
  Call model.get_component("comp1").get_geom("geom1").get_feature("wp1").geom().get_run("pol1")
  Call model.get_component("comp1").get_geom("geom1").get_run("wp1")
  Call model.get_component("comp1").get_geom("geom1").feature().create("rev1", "Revolve")
  Call model.get_component("comp1").get_geom("geom1").get_feature("rev1").set("angtype", "full")
  Call model.get_component("comp1").get_geom("geom1").get_feature("rev1").set("origfaces", False)
  Call model.get_component("comp1").get_geom("geom1").get_run("rev1")
  Call model.get_component("comp1").get_geom("geom1").create("cyl1", "Cylinder")
  Call model.get_component("comp1").get_geom("geom1").get_feature("cyl1").set("r", "r_in")
  Call model.get_component("comp1").get_geom("geom1").get_feature("cyl1").set("h", 0.5)

  Dim temp0(0 to 2) as Double
  temp0(0) = 0.3215
  temp0(1) = -0.5
  temp0(2) = 0.9

  Call model.get_component("comp1").get_geom("geom1").get_feature("cyl1").set("pos", temp0)
  Call model.get_component("comp1").get_geom("geom1").get_feature("cyl1").set("axistype", "y")
  Call model.get_component("comp1").get_geom("geom1").get_run("cyl1")
  Call model.get_component("comp1").get_geom("geom1").create("cyl2", "Cylinder")
  Call model.get_component("comp1").get_geom("geom1").get_feature("cyl2").set("r", "r_in")
  Call model.get_component("comp1").get_geom("geom1").get_feature("cyl2").set("h", 0.5)

  Dim temp1(0 to 2) as Double
  temp1(0) = -0.3215
  temp1(1) = 0
  temp1(2) = 0.9

  Call model.get_component("comp1").get_geom("geom1").get_feature("cyl2").set("pos", temp1)
  Call model.get_component("comp1").get_geom("geom1").get_feature("cyl2").set("axistype", "y")
  Call model.get_component("comp1").get_geom("geom1").get_run("fin")
  Call model.get_component("comp1").get_geom("geom1").get_run("fin")
  Call model.get_component("comp1").get_geom("geom1").create("cmd1", "CompositeDomains")

  Dim temp2(0 to 4) as Long
  temp2(0) = 1
  temp2(1) = 2
  temp2(2) = 3
  temp2(3) = 4
  temp2(4) = 5

  Call model.get_component("comp1").get_geom("geom1").get_feature("cmd1").get_selection("input").set("fin", temp2)
  Call model.get_component("comp1").get_geom("geom1").get_run("cmd1")

  Call model.get_component("comp1").material().create("mat1", "Common")

  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").func().create("eta", "Piecewise")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").func().create("Cp", "Piecewise")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").func().create("rho", "Piecewise")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").func().create("k", "Piecewise")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").func().create("cs", "Interpolation")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").func().create("an1", "Analytic")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").func().create("an2", "Analytic")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").func().create("an3", "Analytic")
  Call model.get_component("comp1").get_material("mat1").label("Water, liquid")
  Call model.get_component("comp1").get_material("mat1").set("family", "water")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("eta").set("arg", "T")

  Dim temp3(0 to 1, 0 to 2) as String
  temp3(0, 0) = "273.15"
  temp3(0, 1) = "413.15"
  temp3(0, 2) = "1.3799566804-0.021224019151*T^1+1.3604562827E-4*T^2-4.6454090319E-7*T^3+8.9042735735E-10*T^4-9.0790692686E-13*T^5+3.8457331488E-16*T^6"
  temp3(1, 0) = "413.15"
  temp3(1, 1) = "553.75"
  temp3(1, 2) = "0.00401235783-2.10746715E-5*T^1+3.85772275E-8*T^2-2.39730284E-11*T^3"

  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("eta").set("pieces", temp3)
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("eta").set("argunit", "K")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("eta").set("fununit", "Pa*s")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("Cp").set("arg", "T")

  Dim temp4(0 to 0, 0 to 2) as String
  temp4(0, 0) = "273.15"
  temp4(0, 1) = "553.75"
  temp4(0, 2) = "12010.1471-80.4072879*T^1+0.309866854*T^2-5.38186884E-4*T^3+3.62536437E-7*T^4"

  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("Cp").set("pieces", temp4)
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("Cp").set("argunit", "K")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("Cp").set("fununit", "J/(kg*K)")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("rho").set("arg", "T")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("rho").set("smooth", "contd1")

  Dim temp5(0 to 1, 0 to 2) as String
  temp5(0, 0) = "273.15"
  temp5(0, 1) = "293.15"
  temp5(0, 2) = "0.000063092789034*T^3-0.060367639882855*T^2+18.9229382407066*T-950.704055329848"
  temp5(1, 0) = "293.15"
  temp5(1, 1) = "373.15"
  temp5(1, 2) = "0.000010335053319*T^3-0.013395065634452*T^2+4.969288832655160*T+432.257114008512"

  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("rho").set("pieces", temp5)
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("rho").set("argunit", "K")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("rho").set("fununit", "kg/m^3")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("k").set("arg", "T")

  Dim temp6(0 to 0, 0 to 2) as String
  temp6(0, 0) = "273.15"
  temp6(0, 1) = "553.75"
  temp6(0, 2) = "-0.869083936+0.00894880345*T^1-1.58366345E-5*T^2+7.97543259E-9*T^3"

  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("k").set("pieces", temp6)
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("k").set("argunit", "K")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("k").set("fununit", "W/(m*K)")

  Dim temp7(0 to 11, 0 to 1) as String
  temp7(0, 0) = "273"
  temp7(0, 1) = "1403"
  temp7(1, 0) = "278"
  temp7(1, 1) = "1427"
  temp7(2, 0) = "283"
  temp7(2, 1) = "1447"
  temp7(3, 0) = "293"
  temp7(3, 1) = "1481"
  temp7(4, 0) = "303"
  temp7(4, 1) = "1507"
  temp7(5, 0) = "313"
  temp7(5, 1) = "1526"
  temp7(6, 0) = "323"
  temp7(6, 1) = "1541"
  temp7(7, 0) = "333"
  temp7(7, 1) = "1552"
  temp7(8, 0) = "343"
  temp7(8, 1) = "1555"
  temp7(9, 0) = "353"
  temp7(9, 1) = "1555"
  temp7(10, 0) = "363"
  temp7(10, 1) = "1550"
  temp7(11, 0) = "373"
  temp7(11, 1) = "1543"

  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("cs").set("table", temp7)
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("cs").set("interp", "piecewisecubic")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("cs").set("argunit", "K")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("cs").set("fununit", "m/s")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("an1").set("funcname", "alpha_p")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("an1").set("expr", "-1/rho(T)*d(rho(T),T)")

  Dim temp8(0 to 0) as String
  temp8(0) = "T"

  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("an1").set("args", temp8)
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("an1").set("argunit", "K")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("an1").set("fununit", "1/K")

  Dim temp9(0 to 0, 0 to 2) as String
  temp9(0, 0) = "T"
  temp9(0, 1) = "273.15"
  temp9(0, 2) = "373.15"

  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("an1").set("plotargs", temp9)
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("an2").set("funcname", "gamma_w")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("an2").set("expr", "1+(T/Cp(T))*(alpha_p(T)*cs(T))^2")

  Dim temp10(0 to 0) as String
  temp10(0) = "T"

  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("an2").set("args", temp10)
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("an2").set("argunit", "K")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("an2").set("fununit", "1")

  Dim temp11(0 to 0, 0 to 2) as String
  temp11(0, 0) = "T"
  temp11(0, 1) = "273.15"
  temp11(0, 2) = "373.15"

  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("an2").set("plotargs", temp11)
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("an3").set("funcname", "muB")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("an3").set("expr", "2.79*eta(T)")

  Dim temp12(0 to 0) as String
  temp12(0) = "T"

  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("an3").set("args", temp12)
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("an3").set("argunit", "K")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("an3").set("fununit", "Pa*s")

  Dim temp13(0 to 0, 0 to 2) as String
  temp13(0, 0) = "T"
  temp13(0, 1) = "273.15"
  temp13(0, 2) = "553.75"

  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").get_func("an3").set("plotargs", temp13)
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").set("thermalexpansioncoefficient", "")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").set("bulkviscosity", "")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").set("dynamicviscosity", "eta(T)")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").set("ratioofspecificheat", "gamma_w(T)")

  Dim temp14(0 to 8) as String
  temp14(0) = "5.5e-6[S/m]"
  temp14(1) = "0"
  temp14(2) = "0"
  temp14(3) = "0"
  temp14(4) = "5.5e-6[S/m]"
  temp14(5) = "0"
  temp14(6) = "0"
  temp14(7) = "0"
  temp14(8) = "5.5e-6[S/m]"

  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").set("electricconductivity", temp14)
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").set("heatcapacity", "Cp(T)")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").set("density", "rho(T)")

  Dim temp15(0 to 8) as String
  temp15(0) = "k(T)"
  temp15(1) = "0"
  temp15(2) = "0"
  temp15(3) = "0"
  temp15(4) = "k(T)"
  temp15(5) = "0"
  temp15(6) = "0"
  temp15(7) = "0"
  temp15(8) = "k(T)"

  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").set("thermalconductivity", temp15)
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").set("soundspeed", "cs(T)")

  Dim temp16(0 to 8) as String
  temp16(0) = "alpha_p(T)"
  temp16(1) = "0"
  temp16(2) = "0"
  temp16(3) = "0"
  temp16(4) = "alpha_p(T)"
  temp16(5) = "0"
  temp16(6) = "0"
  temp16(7) = "0"
  temp16(8) = "alpha_p(T)"

  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").set("thermalexpansioncoefficient", temp16)
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").set("bulkviscosity", "muB(T)")
  Call model.get_component("comp1").get_material("mat1").get_propertyGroup("def").addInput("temperature")
  Call model.get_component("comp1").get_material("mat1").set("family", "water")

  Call model.get_component("comp1").get_physics("spf").create("inl1", "InletBoundary", 2)

  Dim temp17(0 to 1) as Long
  temp17(0) = 9
  temp17(1) = 40

  Call model.get_component("comp1").get_physics("spf").get_feature("inl1").selection().set(temp17)
  Call model.get_component("comp1").get_physics("spf").get_feature("inl1").set("U0in", "u_in")
  Call model.get_component("comp1").get_physics("spf").get_feature("inl1").set("IT", 0.05)
  Call model.get_component("comp1").get_physics("spf").get_feature("inl1").set("LT", "0.07*2*r_in")
  Call model.get_component("comp1").get_physics("spf").create("out1", "OutletBoundary", 2)

  Dim temp18(0 to 3) as Long
  temp18(0) = 20
  temp18(1) = 21
  temp18(2) = 29
  temp18(3) = 30

  Call model.get_component("comp1").get_physics("spf").get_feature("out1").selection().set(temp18)
  Call model.get_component("comp1").get_physics("spf").get_feature("out1").set("BoundaryCondition", "Velocity")
  Call model.get_component("comp1").get_physics("spf").get_feature("out1").set("U0out", "u_out")
  Call model.get_component("comp1").get_physics("spf").create("out2", "OutletBoundary", 2)

  Dim temp19(0 to 3) as Long
  temp19(0) = 18
  temp19(1) = 19
  temp19(2) = 28
  temp19(3) = 31

  Call model.get_component("comp1").get_physics("spf").get_feature("out2").selection().set(temp19)
  Call model.get_component("comp1").get_physics("spf").get_feature("out2").set("NormalFlow", True)
  Call model.get_component("comp1").get_physics("spf").get_feature("out2").set("SuppressBackflow", False)

  Call model.get_component("comp1").get_mesh("mesh1").automatic(False)
  Call model.get_component("comp1").get_mesh("mesh1").get_feature("size").set("hmax", 0.03)
  Call model.get_component("comp1").get_mesh("mesh1").get_feature("size").set("hmin", 0.002)
  Call model.get_component("comp1").get_mesh("mesh1").get_feature("size1").set("custom", True)
  Call model.get_component("comp1").get_mesh("mesh1").get_feature("size1").set("hmaxactive", True)
  Call model.get_component("comp1").get_mesh("mesh1").get_feature("size1").set("hmax", 0.03)
  Call model.get_component("comp1").get_mesh("mesh1").get_feature("size1").set("hminactive", True)
  Call model.get_component("comp1").get_mesh("mesh1").get_feature("size1").set("hmin", 0.001)
  Call model.get_component("comp1").get_mesh("mesh1").get_feature("bl1").get_feature("blp1").set("blnlayers", 9)
  Call model.get_component("comp1").get_mesh("mesh1").get_feature("bl1").get_feature("blp1").set("blstretch", 1.1)
  Call model.get_component("comp1").get_mesh("mesh1").get_feature("bl1").create("blp2", "BndLayerProp")
  Call model.get_component("comp1").get_mesh("mesh1").get_feature("bl1").get_feature("blp2").set("blnlayers", 12)

  Dim temp20(0 to 11) as Long
  temp20(0) = 1
  temp20(1) = 2
  temp20(2) = 7
  temp20(3) = 8
  temp20(4) = 10
  temp20(5) = 11
  temp20(6) = 22
  temp20(7) = 37
  temp20(8) = 38
  temp20(9) = 39
  temp20(10) = 41
  temp20(11) = 42

  Call model.get_component("comp1").get_mesh("mesh1").get_feature("bl1").get_feature("blp2").selection().set(temp20)
  Call model.get_component("comp1").get_mesh("mesh1").run()

  Call model.sol().create("sol1")

  Call model.get_sol("sol1").study("std1")

  Call model.get_study("std1").get_feature("wdi").set("notlistsolnum", 1)
  Call model.get_study("std1").get_feature("wdi").set("notsolnum", "auto")
  Call model.get_study("std1").get_feature("wdi").set("listsolnum", 1)
  Call model.get_study("std1").get_feature("wdi").set("solnum", "auto")
  Call model.get_study("std1").get_feature("stat").set("notlistsolnum", 1)
  Call model.get_study("std1").get_feature("stat").set("notsolnum", "auto")
  Call model.get_study("std1").get_feature("stat").set("listsolnum", 1)
  Call model.get_study("std1").get_feature("stat").set("solnum", "auto")

  Call model.get_sol("sol1").create("st1", "StudyStep")
  Call model.get_sol("sol1").get_feature("st1").set("study", "std1")
  Call model.get_sol("sol1").get_feature("st1").set("studystep", "wdi")
  Call model.get_sol("sol1").create("v1", "Variables")
  Call model.get_sol("sol1").get_feature("v1").set("control", "wdi")
  Call model.get_sol("sol1").create("s1", "Stationary")
  Call model.get_sol("sol1").get_feature("s1").set("stol", 1.0E-6)
  Call model.get_sol("sol1").get_feature("s1").create("fc1", "FullyCoupled")
  Call model.get_sol("sol1").get_feature("s1").get_feature("fc1").set("dtech", "auto")
  Call model.get_sol("sol1").get_feature("s1").get_feature("fc1").set("initstep", 0.01)
  Call model.get_sol("sol1").get_feature("s1").get_feature("fc1").set("minstep", 1.0E-6)
  Call model.get_sol("sol1").get_feature("s1").get_feature("fc1").set("maxiter", 50)
  Call model.get_sol("sol1").get_feature("s1").create("i1", "Iterative")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").set("linsolver", "gmres")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").set("prefuntype", "left")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").set("itrestart", 50)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").set("rhob", 20)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").set("maxlinit", 200)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").set("nlinnormuse", "on")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").label("Algebraic Multigrid Solver (spf)")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").create("mg1", "Multigrid")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").set("prefun", "saamg")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").set("mgcycle", "v")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").set("maxcoarsedof", 50000)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").set("strconn", 0.01)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").set("usesmooth", False)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").set("saamgcompwise", True)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("pr").create("sl1", "SORLine")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linesweeptype", "ssor")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linerelax", 0.7)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("pr").get_feature("sl1").set("iter", 0)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linealgorithm", "mesh")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linemethod", "uncoupled")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("pr").get_feature("sl1").set("seconditer", 1)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("pr").get_feature("sl1").set("relax", 0.5)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("po").create("sl1", "SORLine")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("po").get_feature("sl1").set("linesweeptype", "ssor")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("po").get_feature("sl1").set("linerelax", 0.7)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("po").get_feature("sl1").set("iter", 1)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("po").get_feature("sl1").set("linealgorithm", "mesh")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("po").get_feature("sl1").set("linemethod", "uncoupled")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("po").get_feature("sl1").set("seconditer", 1)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("po").get_feature("sl1").set("relax", 0.5)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("cs").create("d1", "Direct")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("cs").get_feature("d1").set("linsolver", "pardiso")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("cs").get_feature("d1").set("pivotperturb", 1.0E-13)
  Call model.get_sol("sol1").get_feature("s1").create("i2", "Iterative")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").set("linsolver", "gmres")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").set("prefuntype", "left")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").set("itrestart", 50)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").set("rhob", 20)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").set("maxlinit", 200)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").set("nlinnormuse", "on")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").label("Geometric Multigrid Solver (spf)")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").create("mg1", "Multigrid")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").set("prefun", "gmg")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").set("mcasegen", "any")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").set("gmglevels", 1)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("pr").create("sl1", "SORLine")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linesweeptype", "ssor")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linerelax", 0.7)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("pr").get_feature("sl1").set("iter", 0)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linealgorithm", "mesh")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linemethod", "uncoupled")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("pr").get_feature("sl1").set("seconditer", 1)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("pr").get_feature("sl1").set("relax", 0.5)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("po").create("sl1", "SORLine")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("po").get_feature("sl1").set("linesweeptype", "ssor")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("po").get_feature("sl1").set("linerelax", 0.7)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("po").get_feature("sl1").set("iter", 1)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("po").get_feature("sl1").set("linealgorithm", "mesh")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("po").get_feature("sl1").set("linemethod", "uncoupled")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("po").get_feature("sl1").set("seconditer", 1)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("po").get_feature("sl1").set("relax", 0.5)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("cs").create("d1", "Direct")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("cs").get_feature("d1").set("linsolver", "pardiso")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("cs").get_feature("d1").set("pivotperturb", 1.0E-13)
  Call model.get_sol("sol1").get_feature("s1").get_feature("fc1").set("linsolver", "i1")
  Call model.get_sol("sol1").get_feature("s1").get_feature("fc1").set("dtech", "auto")
  Call model.get_sol("sol1").get_feature("s1").get_feature("fc1").set("initstep", 0.01)
  Call model.get_sol("sol1").get_feature("s1").get_feature("fc1").set("minstep", 1.0E-6)
  Call model.get_sol("sol1").get_feature("s1").get_feature("fc1").set("maxiter", 50)
  Call model.get_sol("sol1").get_feature("s1").feature().remove("fcDef")
  Call model.get_sol("sol1").create("su1", "StoreSolution")
  Call model.get_sol("sol1").create("st2", "StudyStep")
  Call model.get_sol("sol1").get_feature("st2").set("study", "std1")
  Call model.get_sol("sol1").get_feature("st2").set("studystep", "stat")
  Call model.get_sol("sol1").create("v2", "Variables")
  Call model.get_sol("sol1").get_feature("v2").set("initmethod", "sol")
  Call model.get_sol("sol1").get_feature("v2").set("initsol", "sol1")
  Call model.get_sol("sol1").get_feature("v2").set("notsolmethod", "sol")
  Call model.get_sol("sol1").get_feature("v2").set("notsol", "sol1")
  Call model.get_sol("sol1").get_feature("v2").set("control", "stat")
  Call model.get_sol("sol1").create("s2", "Stationary")
  Call model.get_sol("sol1").get_feature("s2").get_feature("aDef").set("cachepattern", True)
  Call model.get_sol("sol1").get_feature("s2").create("se1", "Segregated")
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").feature().remove("ssDef")
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").create("ss1", "SegregatedStep")

  Dim temp21(0 to 1) as String
  temp21(0) = "comp1_u"
  temp21(1) = "comp1_p"

  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").get_feature("ss1").set("segvar", temp21)
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").get_feature("ss1").set("subdamp", 0.5)
  Call model.get_sol("sol1").get_feature("s2").create("i1", "Iterative")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").set("linsolver", "gmres")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").set("prefuntype", "left")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").set("itrestart", 50)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").set("rhob", 20)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").set("maxlinit", 200)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").set("nlinnormuse", "on")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").label("Algebraic Multigrid Solver (spf)")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").create("mg1", "Multigrid")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").set("prefun", "saamg")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").set("mgcycle", "f")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").set("maxcoarsedof", 80000)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").set("strconn", 0.02)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").set("usesmooth", False)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").set("saamgcompwise", True)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").get_feature("pr").create("sc1", "SCGS")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").get_feature("pr").get_feature("sc1").set("linesweeptype", "ssor")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").get_feature("pr").get_feature("sc1").set("iter", 0)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").get_feature("pr").get_feature("sc1").set("scgsrelax", 0.7)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").get_feature("pr").get_feature("sc1").set("scgsmethod", "lines_vertices")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").get_feature("pr").get_feature("sc1").set("scgsvertexrelax", 0.7)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").get_feature("pr").get_feature("sc1").set("seconditer", 1)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").get_feature("pr").get_feature("sc1").set("relax", 0.5)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").get_feature("po").create("sc1", "SCGS")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").get_feature("po").get_feature("sc1").set("linesweeptype", "ssor")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").get_feature("po").get_feature("sc1").set("iter", 1)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").get_feature("po").get_feature("sc1").set("scgsrelax", 0.7)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").get_feature("po").get_feature("sc1").set("scgsmethod", "lines_vertices")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").get_feature("po").get_feature("sc1").set("scgsvertexrelax", 0.7)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").get_feature("po").get_feature("sc1").set("seconditer", 1)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").get_feature("po").get_feature("sc1").set("relax", 0.5)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").get_feature("cs").create("d1", "Direct")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").get_feature("cs").get_feature("d1").set("linsolver", "pardiso")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").get_feature("cs").get_feature("d1").set("pivotperturb", 1.0E-13)
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").get_feature("ss1").set("linsolver", "i1")
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").get_feature("ss1").label("Velocity u, Pressure p")
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").create("ss2", "SegregatedStep")

  Dim temp22(0 to 2) as String
  temp22(0) = "comp1_k"
  temp22(1) = "comp1_ep"
  temp22(2) = "comp1_zeta"

  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").get_feature("ss2").set("segvar", temp22)
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").get_feature("ss2").set("subdamp", 0.35)
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").get_feature("ss2").set("subiter", 3)
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").get_feature("ss2").set("subtermconst", "itertol")
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").get_feature("ss2").set("subntolfact", 1)
  Call model.get_sol("sol1").get_feature("s2").create("i2", "Iterative")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").set("linsolver", "gmres")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").set("prefuntype", "left")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").set("itrestart", 50)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").set("rhob", 20)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").set("maxlinit", 200)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").set("nlinnormuse", "on")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").label("Algebraic Multigrid Solver")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").create("mg1", "Multigrid")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").set("prefun", "saamg")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").set("mgcycle", "v")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").set("maxcoarsedof", 50000)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").set("strconn", 0.01)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").set("usesmooth", False)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").set("saamgcompwise", True)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").get_feature("pr").create("sl1", "SORLine")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linesweeptype", "ssor")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linerelax", 0.7)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").get_feature("pr").get_feature("sl1").set("iter", 0)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linealgorithm", "mesh")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linemethod", "uncoupled")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").get_feature("pr").get_feature("sl1").set("seconditer", 1)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").get_feature("pr").get_feature("sl1").set("relax", 0.5)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").get_feature("po").create("sl1", "SORLine")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").get_feature("po").get_feature("sl1").set("linesweeptype", "ssor")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").get_feature("po").get_feature("sl1").set("linerelax", 0.7)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").get_feature("po").get_feature("sl1").set("iter", 1)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").get_feature("po").get_feature("sl1").set("linealgorithm", "mesh")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").get_feature("po").get_feature("sl1").set("linemethod", "uncoupled")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").get_feature("po").get_feature("sl1").set("seconditer", 1)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").get_feature("po").get_feature("sl1").set("relax", 0.5)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").get_feature("cs").create("d1", "Direct")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").get_feature("cs").get_feature("d1").set("linsolver", "pardiso")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").get_feature("cs").get_feature("d1").set("pivotperturb", 1.0E-13)
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").get_feature("ss2").set("linsolver", "i2")
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").get_feature("ss2").label("Turbulence variables")
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").create("ss3", "SegregatedStep")

  Dim temp23(0 to 0) as String
  temp23(0) = "comp1_alpha"

  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").get_feature("ss3").set("segvar", temp23)
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").get_feature("ss3").set("subdamp", 1)
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").get_feature("ss3").set("subtermconst", "iter")
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").get_feature("ss3").set("subiter", 1)
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").get_feature("ss3").set("subntolfact", 1)
  Call model.get_sol("sol1").get_feature("s2").create("i3", "Iterative")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").set("linsolver", "gmres")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").set("prefuntype", "left")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").set("itrestart", 50)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").set("rhob", 20)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").set("maxlinit", 200)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").set("nlinnormuse", "on")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").label("Algebraic Multigrid Solver 2")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").create("mg1", "Multigrid")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").set("prefun", "saamg")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").set("mgcycle", "v")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").set("maxcoarsedof", 50000)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").set("strconn", 0.01)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").set("usesmooth", False)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").set("saamgcompwise", True)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").get_feature("pr").create("sl1", "SORLine")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linesweeptype", "ssor")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linerelax", 0.7)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").get_feature("pr").get_feature("sl1").set("iter", 0)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linealgorithm", "mesh")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linemethod", "uncoupled")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").get_feature("pr").get_feature("sl1").set("seconditer", 1)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").get_feature("pr").get_feature("sl1").set("relax", 0.5)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").get_feature("po").create("sl1", "SORLine")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").get_feature("po").get_feature("sl1").set("linesweeptype", "ssor")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").get_feature("po").get_feature("sl1").set("linerelax", 0.7)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").get_feature("po").get_feature("sl1").set("iter", 1)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").get_feature("po").get_feature("sl1").set("linealgorithm", "mesh")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").get_feature("po").get_feature("sl1").set("linemethod", "uncoupled")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").get_feature("po").get_feature("sl1").set("seconditer", 1)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").get_feature("po").get_feature("sl1").set("relax", 0.5)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").get_feature("cs").create("d1", "Direct")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").get_feature("cs").get_feature("d1").set("linsolver", "pardiso")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").get_feature("cs").get_feature("d1").set("pivotperturb", 1.0E-13)
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").get_feature("ss3").set("linsolver", "i3")
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").get_feature("ss3").label("Elliptic blending function")
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").set("segstabacc", "segcflcmp")
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").set("subinitcfl", 2)
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").set("subkppid", 0.65)
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").set("subkdpid", 0.03)
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").set("subkipid", 0.05)
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").set("subcfltol", 0.08)
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").set("maxsegiter", 400)
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").create("ll1", "LowerLimit")
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").get_feature("ll1").set("lowerlimit", "comp1.k 0 comp1.ep 0 comp1.zeta 0 comp1.alpha 0 ")
  Call model.get_sol("sol1").get_feature("s2").create("i4", "Iterative")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").set("linsolver", "gmres")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").set("prefuntype", "left")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").set("itrestart", 50)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").set("rhob", 20)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").set("maxlinit", 200)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").set("nlinnormuse", "on")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").label("Geometric Multigrid Solver (spf)")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").create("mg1", "Multigrid")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").get_feature("mg1").set("prefun", "gmg")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").get_feature("mg1").set("mcasegen", "any")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").get_feature("mg1").set("gmglevels", 2)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").get_feature("mg1").get_feature("pr").create("sc1", "SCGS")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").get_feature("mg1").get_feature("pr").get_feature("sc1").set("linesweeptype", "ssor")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").get_feature("mg1").get_feature("pr").get_feature("sc1").set("iter", 0)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").get_feature("mg1").get_feature("pr").get_feature("sc1").set("scgsrelax", 0.7)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").get_feature("mg1").get_feature("pr").get_feature("sc1").set("scgsmethod", "lines_vertices")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").get_feature("mg1").get_feature("pr").get_feature("sc1").set("scgsvertexrelax", 0.7)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").get_feature("mg1").get_feature("pr").get_feature("sc1").set("seconditer", 1)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").get_feature("mg1").get_feature("pr").get_feature("sc1").set("relax", 0.5)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").get_feature("mg1").get_feature("po").create("sc1", "SCGS")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").get_feature("mg1").get_feature("po").get_feature("sc1").set("linesweeptype", "ssor")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").get_feature("mg1").get_feature("po").get_feature("sc1").set("iter", 1)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").get_feature("mg1").get_feature("po").get_feature("sc1").set("scgsrelax", 0.7)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").get_feature("mg1").get_feature("po").get_feature("sc1").set("scgsmethod", "lines_vertices")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").get_feature("mg1").get_feature("po").get_feature("sc1").set("scgsvertexrelax", 0.7)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").get_feature("mg1").get_feature("po").get_feature("sc1").set("seconditer", 1)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").get_feature("mg1").get_feature("po").get_feature("sc1").set("relax", 0.5)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").get_feature("mg1").get_feature("cs").create("d1", "Direct")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").get_feature("mg1").get_feature("cs").get_feature("d1").set("linsolver", "pardiso")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").get_feature("mg1").get_feature("cs").get_feature("d1").set("pivotperturb", 1.0E-13)
  Call model.get_sol("sol1").get_feature("s2").create("i5", "Iterative")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").set("linsolver", "gmres")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").set("prefuntype", "left")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").set("itrestart", 50)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").set("rhob", 20)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").set("maxlinit", 200)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").set("nlinnormuse", "on")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").label("Geometric Multigrid Solver")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").create("mg1", "Multigrid")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").get_feature("mg1").set("prefun", "gmg")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").get_feature("mg1").set("mcasegen", "any")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").get_feature("mg1").set("gmglevels", 1)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").get_feature("mg1").get_feature("pr").create("sl1", "SORLine")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linesweeptype", "ssor")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linerelax", 0.7)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").get_feature("mg1").get_feature("pr").get_feature("sl1").set("iter", 0)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linealgorithm", "mesh")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linemethod", "uncoupled")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").get_feature("mg1").get_feature("pr").get_feature("sl1").set("seconditer", 1)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").get_feature("mg1").get_feature("pr").get_feature("sl1").set("relax", 0.5)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").get_feature("mg1").get_feature("po").create("sl1", "SORLine")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").get_feature("mg1").get_feature("po").get_feature("sl1").set("linesweeptype", "ssor")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").get_feature("mg1").get_feature("po").get_feature("sl1").set("linerelax", 0.7)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").get_feature("mg1").get_feature("po").get_feature("sl1").set("iter", 1)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").get_feature("mg1").get_feature("po").get_feature("sl1").set("linealgorithm", "mesh")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").get_feature("mg1").get_feature("po").get_feature("sl1").set("linemethod", "uncoupled")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").get_feature("mg1").get_feature("po").get_feature("sl1").set("seconditer", 1)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").get_feature("mg1").get_feature("po").get_feature("sl1").set("relax", 0.5)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").get_feature("mg1").get_feature("cs").create("d1", "Direct")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").get_feature("mg1").get_feature("cs").get_feature("d1").set("linsolver", "pardiso")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").get_feature("mg1").get_feature("cs").get_feature("d1").set("pivotperturb", 1.0E-13)
  Call model.get_sol("sol1").get_feature("s2").create("i6", "Iterative")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").set("linsolver", "gmres")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").set("prefuntype", "left")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").set("itrestart", 50)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").set("rhob", 20)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").set("maxlinit", 200)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").set("nlinnormuse", "on")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").label("Geometric Multigrid Solver 2")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").create("mg1", "Multigrid")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").get_feature("mg1").set("prefun", "gmg")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").get_feature("mg1").set("mcasegen", "any")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").get_feature("mg1").set("gmglevels", 1)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").get_feature("mg1").get_feature("pr").create("sl1", "SORLine")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linesweeptype", "ssor")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linerelax", 0.7)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").get_feature("mg1").get_feature("pr").get_feature("sl1").set("iter", 0)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linealgorithm", "mesh")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linemethod", "uncoupled")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").get_feature("mg1").get_feature("pr").get_feature("sl1").set("seconditer", 1)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").get_feature("mg1").get_feature("pr").get_feature("sl1").set("relax", 0.5)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").get_feature("mg1").get_feature("po").create("sl1", "SORLine")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").get_feature("mg1").get_feature("po").get_feature("sl1").set("linesweeptype", "ssor")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").get_feature("mg1").get_feature("po").get_feature("sl1").set("linerelax", 0.7)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").get_feature("mg1").get_feature("po").get_feature("sl1").set("iter", 1)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").get_feature("mg1").get_feature("po").get_feature("sl1").set("linealgorithm", "mesh")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").get_feature("mg1").get_feature("po").get_feature("sl1").set("linemethod", "uncoupled")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").get_feature("mg1").get_feature("po").get_feature("sl1").set("seconditer", 1)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").get_feature("mg1").get_feature("po").get_feature("sl1").set("relax", 0.5)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").get_feature("mg1").get_feature("cs").create("d1", "Direct")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").get_feature("mg1").get_feature("cs").get_feature("d1").set("linsolver", "pardiso")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").get_feature("mg1").get_feature("cs").get_feature("d1").set("pivotperturb", 1.0E-13)
  Call model.get_sol("sol1").get_feature("s2").feature().remove("fcDef")
  Call model.get_sol("sol1").get_feature("v2").set("notsolnum", "auto")
  Call model.get_sol("sol1").get_feature("v2").set("notsolvertype", "solnum")

  Dim temp24(0 to 0) as String
  temp24(0) = "1"

  Call model.get_sol("sol1").get_feature("v2").set("notlistsolnum", temp24)
  Call model.get_sol("sol1").get_feature("v2").set("notsolnum", "auto")

  Dim temp25(0 to 0) as String
  temp25(0) = "1"

  Call model.get_sol("sol1").get_feature("v2").set("notlistsolnum", temp25)
  Call model.get_sol("sol1").get_feature("v2").set("notsolnum", "auto")
  Call model.get_sol("sol1").get_feature("v2").set("control", "stat")
  Call model.get_sol("sol1").get_feature("v2").set("solnum", "auto")
  Call model.get_sol("sol1").get_feature("v2").set("solvertype", "solnum")

  Dim temp26(0 to 0) as String
  temp26(0) = "1"

  Call model.get_sol("sol1").get_feature("v2").set("listsolnum", temp26)
  Call model.get_sol("sol1").get_feature("v2").set("solnum", "auto")

  Dim temp27(0 to 0) as String
  temp27(0) = "1"

  Call model.get_sol("sol1").get_feature("v2").set("listsolnum", temp27)
  Call model.get_sol("sol1").get_feature("v2").set("solnum", "auto")
  Call model.get_sol("sol1").attach("std1")

  Call model.result().get_dataset("dset1").set("geom", "geom1")
  Call model.result().create("pg1", "PlotGroup3D")

  Call model.get_result("pg1").label("Velocity (spf)")
  Call model.get_result("pg1").set("frametype", "spatial")
  Call model.get_result("pg1").set("data", "dset1")
  Call model.get_result("pg1").feature().create("slc1", "Slice")
  Call model.get_result("pg1").get_feature("slc1").label("Slice")
  Call model.get_result("pg1").get_feature("slc1").set("smooth", "internal")
  Call model.get_result("pg1").get_feature("slc1").set("data", "parent")

  Call model.result().dataset().create("surf1", "Surface")
  Call model.result().get_dataset("surf1").label("Exterior Walls")
  Call model.result().get_dataset("surf1").set("data", "dset1")
  Call model.result().get_dataset("surf1").selection().geom("geom1", 2)

  Dim temp28(0 to 31) as Long
  temp28(0) = 1
  temp28(1) = 2
  temp28(2) = 3
  temp28(3) = 4
  temp28(4) = 5
  temp28(5) = 6
  temp28(6) = 7
  temp28(7) = 8
  temp28(8) = 10
  temp28(9) = 11
  temp28(10) = 12
  temp28(11) = 13
  temp28(12) = 14
  temp28(13) = 15
  temp28(14) = 16
  temp28(15) = 17
  temp28(16) = 22
  temp28(17) = 23
  temp28(18) = 24
  temp28(19) = 25
  temp28(20) = 26
  temp28(21) = 27
  temp28(22) = 32
  temp28(23) = 33
  temp28(24) = 34
  temp28(25) = 35
  temp28(26) = 36
  temp28(27) = 37
  temp28(28) = 38
  temp28(29) = 39
  temp28(30) = 41
  temp28(31) = 42

  Call model.result().get_dataset("surf1").selection().set(temp28)
  Call model.result().get_dataset("surf1").selection().inherit(False)
  Call model.result().create("pg2", "PlotGroup3D")

  Call model.get_result("pg2").label("Pressure (spf)")
  Call model.get_result("pg2").set("frametype", "spatial")
  Call model.get_result("pg2").set("data", "surf1")
  Call model.get_result("pg2").feature().create("surf1", "Surface")
  Call model.get_result("pg2").get_feature("surf1").label("Surface")
  Call model.get_result("pg2").get_feature("surf1").set("expr", "1")
  Call model.get_result("pg2").get_feature("surf1").set("titletype", "none")
  Call model.get_result("pg2").get_feature("surf1").set("coloring", "uniform")
  Call model.get_result("pg2").get_feature("surf1").set("color", "gray")
  Call model.get_result("pg2").get_feature("surf1").set("smooth", "internal")
  Call model.get_result("pg2").get_feature("surf1").set("data", "parent")
  Call model.get_result("pg2").feature().create("con1", "Contour")
  Call model.get_result("pg2").get_feature("con1").label("Pressure")
  Call model.get_result("pg2").get_feature("con1").set("expr", "p")
  Call model.get_result("pg2").get_feature("con1").set("number", 40)
  Call model.get_result("pg2").get_feature("con1").set("smooth", "internal")
  Call model.get_result("pg2").get_feature("con1").set("data", "parent")

  Call model.result().create("pg3", "PlotGroup3D")

  Call model.get_result("pg3").label("Wall Resolution (spf)")
  Call model.get_result("pg3").set("frametype", "spatial")
  Call model.get_result("pg3").set("data", "surf1")
  Call model.get_result("pg3").feature().create("surf1", "Surface")
  Call model.get_result("pg3").get_feature("surf1").label("Wall Resolution")
  Call model.get_result("pg3").get_feature("surf1").set("expr", "spf.Delta_wPlus")
  Call model.get_result("pg3").get_feature("surf1").set("smooth", "internal")
  Call model.get_result("pg3").get_feature("surf1").set("data", "parent")

  Call model.get_sol("sol1").runAll()

  Call model.get_result("pg1").run()
  Call model.get_result("pg2").set("data", "surf1")
  Call model.get_result("pg3").set("data", "surf1")

  Call model.result().dataset().create("surf2", "Surface")

  Dim temp29(0 to 31) as Long
  temp29(0) = 2
  temp29(1) = 4
  temp29(2) = 5
  temp29(3) = 6
  temp29(4) = 7
  temp29(5) = 8
  temp29(6) = 9
  temp29(7) = 10
  temp29(8) = 11
  temp29(9) = 12
  temp29(10) = 13
  temp29(11) = 14
  temp29(12) = 15
  temp29(13) = 16
  temp29(14) = 17
  temp29(15) = 22
  temp29(16) = 23
  temp29(17) = 24
  temp29(18) = 25
  temp29(19) = 26
  temp29(20) = 27
  temp29(21) = 32
  temp29(22) = 33
  temp29(23) = 34
  temp29(24) = 35
  temp29(25) = 36
  temp29(26) = 37
  temp29(27) = 38
  temp29(28) = 39
  temp29(29) = 40
  temp29(30) = 41
  temp29(31) = 42

  Call model.result().get_dataset("surf2").selection().set(temp29)
  Call model.result().dataset().create("edg1", "Edge3D")

  Dim temp30(0 to 51) as Long
  temp30(0) = 1
  temp30(1) = 2
  temp30(2) = 4
  temp30(3) = 5
  temp30(4) = 7
  temp30(5) = 9
  temp30(6) = 10
  temp30(7) = 11
  temp30(8) = 12
  temp30(9) = 13
  temp30(10) = 14
  temp30(11) = 16
  temp30(12) = 17
  temp30(13) = 18
  temp30(14) = 19
  temp30(15) = 21
  temp30(16) = 23
  temp30(17) = 24
  temp30(18) = 25
  temp30(19) = 27
  temp30(20) = 28
  temp30(21) = 29
  temp30(22) = 31
  temp30(23) = 32
  temp30(24) = 34
  temp30(25) = 35
  temp30(26) = 36
  temp30(27) = 37
  temp30(28) = 38
  temp30(29) = 39
  temp30(30) = 40
  temp30(31) = 41
  temp30(32) = 42
  temp30(33) = 43
  temp30(34) = 45
  temp30(35) = 47
  temp30(36) = 49
  temp30(37) = 55
  temp30(38) = 58
  temp30(39) = 59
  temp30(40) = 61
  temp30(41) = 63
  temp30(42) = 65
  temp30(43) = 66
  temp30(44) = 72
  temp30(45) = 73
  temp30(46) = 74
  temp30(47) = 75
  temp30(48) = 78
  temp30(49) = 80
  temp30(50) = 81
  temp30(51) = 82

  Call model.result().get_dataset("edg1").selection().set(temp30)
  Call model.result().dataset().create("cpl1", "CutPlane")
  Call model.result().dataset().create("cpl2", "CutPlane")
  Call model.result().get_dataset("cpl2").set("quickplane", "xz")
  Call model.result().dataset().create("cln1", "CutLine3D")
  Call model.result().get_dataset("cln1").setIndex("genpoints", -0.5, 0, 0)
  Call model.result().get_dataset("cln1").setIndex("genpoints", 0, 1, 0)
  Call model.result().get_dataset("cln1").setIndex("genpoints", 0.6, 0, 2)
  Call model.result().get_dataset("cln1").setIndex("genpoints", 0.6, 1, 2)

  Call model.get_result("pg1").run()
  Call model.get_result("pg1").label("Streamlines")
  Call model.get_result("pg1").set("titletype", "none")
  Call model.get_result("pg1").set("edges", False)
  Call model.get_result("pg1").run()
  Call model.get_result("pg1").feature().remove("slc1")
  Call model.get_result("pg1").run()
  Call model.get_result("pg1").create("surf1", "Surface")
  Call model.get_result("pg1").get_feature("surf1").set("data", "surf2")
  Call model.get_result("pg1").get_feature("surf1").set("coloring", "uniform")
  Call model.get_result("pg1").get_feature("surf1").set("color", "gray")
  Call model.get_result("pg1").run()
  Call model.get_result("pg1").create("line1", "Line")
  Call model.get_result("pg1").get_feature("line1").set("data", "edg1")
  Call model.get_result("pg1").get_feature("line1").set("coloring", "uniform")
  Call model.get_result("pg1").get_feature("line1").set("color", "custom")

  Dim temp31(0 to 2) as Double
  temp31(0) = 0.501960813999176
  temp31(1) = 0.501960813999176
  temp31(2) = 0.501960813999176

  Call model.get_result("pg1").get_feature("line1").set("customcolor", temp31)
  Call model.get_result("pg1").run()

  Call model.get_component("comp1").get_view("view1").set("showgrid", False)

  Call model.get_result("pg1").run()
  Call model.get_result("pg1").create("str1", "Streamline")
  Call model.get_result("pg1").get_feature("str1").set("selnumber", 10)

  Dim temp32(0 to 3) as Long
  temp32(0) = 18
  temp32(1) = 19
  temp32(2) = 28
  temp32(3) = 31

  Call model.get_result("pg1").get_feature("str1").selection().set(temp32)
  Call model.get_result("pg1").get_feature("str1").set("linetype", "tube")
  Call model.get_result("pg1").get_feature("str1").set("radiusexpr", "0.0025")
  Call model.get_result("pg1").get_feature("str1").set("tuberadiusscaleactive", True)
  Call model.get_result("pg1").get_feature("str1").set("color", "custom")

  Dim temp33(0 to 2) as Double
  temp33(0) = 0.501960813999176
  temp33(1) = 0
  temp33(2) = 0.250980406999588

  Call model.get_result("pg1").get_feature("str1").set("customcolor", temp33)
  Call model.get_result("pg1").run()
  Call model.get_result("pg1").create("str2", "Streamline")

  Dim temp34(0 to 3) as Long
  temp34(0) = 20
  temp34(1) = 21
  temp34(2) = 29
  temp34(3) = 30

  Call model.get_result("pg1").get_feature("str2").selection().set(temp34)
  Call model.get_result("pg1").get_feature("str2").set("selnumber", 2)
  Call model.get_result("pg1").get_feature("str2").set("linetype", "tube")
  Call model.get_result("pg1").get_feature("str2").set("radiusexpr", "0.0025")
  Call model.get_result("pg1").get_feature("str2").set("tuberadiusscaleactive", True)
  Call model.get_result("pg1").get_feature("str2").set("color", "custom")

  Dim temp35(0 to 2) as Double
  temp35(0) = 0
  temp35(1) = 0.501960813999176
  temp35(2) = 0.7529411911964417

  Call model.get_result("pg1").get_feature("str2").set("customcolor", temp35)
  Call model.get_result("pg1").run()
  Call model.get_result("pg1").run()
  Call model.get_result("pg2").run()
  Call model.get_result("pg2").set("edges", False)
  Call model.get_result("pg2").run()
  Call model.get_result("pg2").get_feature("surf1").set("data", "surf2")
  Call model.get_result("pg2").run()
  Call model.get_result("pg2").create("line1", "Line")
  Call model.get_result("pg2").get_feature("line1").set("data", "edg1")
  Call model.get_result("pg2").get_feature("line1").set("coloring", "uniform")
  Call model.get_result("pg2").get_feature("line1").set("color", "custom")

  Dim temp36(0 to 2) as Double
  temp36(0) = 0.501960813999176
  temp36(1) = 0.501960813999176
  temp36(2) = 0.501960813999176

  Call model.get_result("pg2").get_feature("line1").set("customcolor", temp36)
  Call model.get_result("pg2").run()
  Call model.get_result("pg2").feature().remove("con1")
  Call model.get_result("pg2").run()
  Call model.get_result("pg2").create("surf2", "Surface")
  Call model.get_result("pg2").get_feature("surf2").set("data", "cpl1")
  Call model.get_result("pg2").get_feature("surf2").set("expr", "p")
  Call model.get_result("pg2").get_feature("surf2").set("rangecoloractive", True)
  Call model.get_result("pg2").get_feature("surf2").set("rangecolormin", -13000)
  Call model.get_result("pg2").get_feature("surf2").set("rangecolormax", 106000)
  Call model.get_result("pg2").get_feature("surf2").set("colortable", "JupiterAuroraBorealis")
  Call model.get_result("pg2").get_feature("surf2").set("colortablerev", True)
  Call model.get_result("pg2").run()
  Call model.get_result("pg2").create("surf3", "Surface")
  Call model.get_result("pg2").get_feature("surf3").set("data", "cpl2")
  Call model.get_result("pg2").get_feature("surf3").set("expr", "p")
  Call model.get_result("pg2").get_feature("surf3").set("rangecoloractive", True)
  Call model.get_result("pg2").get_feature("surf3").set("rangecolormin", -13000)
  Call model.get_result("pg2").get_feature("surf3").set("rangecolormax", 106000)
  Call model.get_result("pg2").get_feature("surf3").set("colorlegend", False)
  Call model.get_result("pg2").get_feature("surf3").set("colortable", "JupiterAuroraBorealis")
  Call model.get_result("pg2").get_feature("surf3").set("colortablerev", True)
  Call model.get_result("pg2").run()
  Call model.get_result("pg2").create("str1", "StreamlineSurface")
  Call model.get_result("pg2").get_feature("str1").set("data", "cpl1")

  Dim temp37(0 to 2) as String
  temp37(0) = "0"
  temp37(1) = "v"
  temp37(2) = "w"

  Call model.get_result("pg2").get_feature("str1").set("expr", temp37)
  Call model.get_result("pg2").get_feature("str1").set("posmethod", "magnitude")
  Call model.get_result("pg2").get_feature("str1").set("mdensity", 16)
  Call model.get_result("pg2").get_feature("str1").set("linetype", "tube")
  Call model.get_result("pg2").get_feature("str1").set("radiusexpr", "0.002")
  Call model.get_result("pg2").get_feature("str1").set("tuberadiusscaleactive", True)
  Call model.get_result("pg2").get_feature("str1").set("color", "gray")
  Call model.get_result("pg2").get_feature("str1").set("linearrowtype", "tangent")
  Call model.get_result("pg2").run()
  Call model.get_result("pg2").create("str2", "StreamlineSurface")
  Call model.get_result("pg2").get_feature("str2").set("data", "cpl2")

  Dim temp38(0 to 2) as String
  temp38(0) = "u"
  temp38(1) = "0"
  temp38(2) = "w"

  Call model.get_result("pg2").get_feature("str2").set("expr", temp38)
  Call model.get_result("pg2").get_feature("str2").set("posmethod", "magnitude")
  Call model.get_result("pg2").get_feature("str2").set("mdensity", 16)
  Call model.get_result("pg2").get_feature("str2").set("linetype", "tube")
  Call model.get_result("pg2").get_feature("str2").set("radiusexpr", "0.002")
  Call model.get_result("pg2").get_feature("str2").set("tuberadiusscaleactive", True)
  Call model.get_result("pg2").get_feature("str2").set("color", "gray")
  Call model.get_result("pg2").get_feature("str2").set("linearrowtype", "tangent")
  Call model.get_result("pg2").run()
  Call model.get_result("pg2").set("titletype", "none")
  Call model.get_result("pg2").run()

  Call model.get_component("comp1").view().create("view3", "geom1")

  Call model.get_component("comp1").get_view("view3").camera().set("zoomanglefull", 6.68947124481201)
  Call model.get_component("comp1").get_view("view3").camera().setIndex("position", -10.114247322082, 0)
  Call model.get_component("comp1").get_view("view3").camera().setIndex("position", -8.2812986373901, 1)
  Call model.get_component("comp1").get_view("view3").camera().setIndex("position", "-2.6796455383300", 2)
  Call model.get_component("comp1").get_view("view3").camera().setIndex("target", -1.907348632812E-6, 0)
  Call model.get_component("comp1").get_view("view3").camera().setIndex("target", -1.23977661132812E-5, 1)
  Call model.get_component("comp1").get_view("view3").camera().setIndex("target", 0.1000037193298339, 2)
  Call model.get_component("comp1").get_view("view3").camera().setIndex("up", "-0.1703266799449920", 0)
  Call model.get_component("comp1").get_view("view3").camera().setIndex("up", -0.120248958468437, 1)
  Call model.get_component("comp1").get_view("view3").camera().setIndex("up", 0.978018164634704, 2)

  Dim temp39(0 to 2) as Double
  temp39(0) = 0
  temp39(1) = 0
  temp39(2) = 0.1000000238418579

  Call model.get_component("comp1").get_view("view3").camera().set("rotationpoint", temp39)
  Call model.get_component("comp1").get_view("view3").camera().setIndex("viewoffset", -0.01035210117697715, 0)
  Call model.get_component("comp1").get_view("view3").camera().setIndex("viewoffset", -0.4598191678524017, 1)
  Call model.get_component("comp1").get_view("view3").set("showgrid", False)
  Call model.get_component("comp1").get_view("view3").set("locked", True)

  Call model.get_result("pg2").run()
  Call model.get_result("pg2").set("view", "view3")

  Call model.result().create("pg4", "PlotGroup3D")

  Call model.get_result("pg4").run()
  Call model.get_result("pg4").label("Vortex core")
  Call model.get_result("pg4").set("titletype", "none")
  Call model.get_result("pg4").set("view", "view1")
  Call model.get_result("pg4").set("edges", False)
  Call model.get_result("pg4").create("surf1", "Surface")
  Call model.get_result("pg4").get_feature("surf1").set("data", "surf2")
  Call model.get_result("pg4").get_feature("surf1").set("coloring", "uniform")
  Call model.get_result("pg4").get_feature("surf1").set("color", "gray")
  Call model.get_result("pg4").run()
  Call model.get_result("pg4").create("line1", "Line")
  Call model.get_result("pg4").get_feature("line1").set("data", "edg1")
  Call model.get_result("pg4").get_feature("line1").set("coloring", "uniform")
  Call model.get_result("pg4").get_feature("line1").set("color", "custom")

  Dim temp40(0 to 2) as Double
  temp40(0) = 0.501960813999176
  temp40(1) = 0.501960813999176
  temp40(2) = 0.501960813999176

  Call model.get_result("pg4").get_feature("line1").set("customcolor", temp40)
  Call model.get_result("pg4").run()
  Call model.get_result("pg4").create("iso1", "Isosurface")
  Call model.get_result("pg4").get_feature("iso1").set("expr", "spf.vorticityz")
  Call model.get_result("pg4").get_feature("iso1").set("descr", "Vorticity field, z component")
  Call model.get_result("pg4").get_feature("iso1").set("levelmethod", "levels")
  Call model.get_result("pg4").get_feature("iso1").set("levels", 90)
  Call model.get_result("pg4").get_feature("iso1").set("interactive", True)
  Call model.get_result("pg4").get_feature("iso1").set("coloring", "uniform")
  Call model.get_result("pg4").get_feature("iso1").set("color", "custom")

  Dim temp41(0 to 2) as Double
  temp41(0) = 0
  temp41(1) = 0.501960813999176
  temp41(2) = 0.7529411911964417

  Call model.get_result("pg4").get_feature("iso1").set("customcolor", temp41)
  Call model.get_result("pg4").get_feature("iso1").set("colorlegend", False)
  Call model.get_result("pg4").run()
  Call model.get_result("pg4").run()

  Call model.result().create("pg5", "PlotGroup1D")

  Call model.get_result("pg5").run()
  Call model.get_result("pg5").label("Swirl velocity")
  Call model.get_result("pg5").set("titletype", "manual")
  Call model.get_result("pg5").set("title", "Swirl Velocity")
  Call model.get_result("pg5").set("xlabelactive", True)
  Call model.get_result("pg5").set("xlabel", "r (m)")
  Call model.get_result("pg5").set("ylabelactive", True)
  Call model.get_result("pg5").set("ylabel", "Swirl velocity (m/s)")
  Call model.get_result("pg5").create("lngr1", "LineGraph")
  Call model.get_result("pg5").run()
  Call model.get_result("pg5").set("data", "cln1")
  Call model.get_result("pg5").run()
  Call model.get_result("pg5").get_feature("lngr1").set("expr", "(x*v-y*u)/sqrt(x^2+y^2+eps)")
  Call model.get_result("pg5").get_feature("lngr1").set("xdata", "expr")
  Call model.get_result("pg5").get_feature("lngr1").set("xdataexpr", "-x")
  Call model.get_result("pg5").run()
  Call model.get_result("pg1").run()
  Call model.get_result("pg1").set("view", "view1")
  Call model.get_result("pg3").run()
  Call model.get_result("pg3").set("view", "view1")
  Call model.get_result("pg2").run()
  Call model.get_result("pg2").set("view", "view1")

  Call model.comments("Flow in a Hydrocyclone" & vbNewLine & "" & vbNewLine & "In this tutorial model, anisotropic turbulence in a hydrocyclone is simulated using the v2-f turbulence model. The complex swirl, which can be divided into an outer semi-free vortex and an inner core of solid body rotation, is captured as a result of the damping of turbulence fluctuations in the wall-normal direction. The velocity field and pressure drops between the inlets and the two outlets are both in qualitative and quantitative agreement with results from similar designs in literature.")

  Call model.mesh().clearMeshes()

  Call model.get_sol("sol1").clearSolutionData()
  Call model.get_sol("sol2").clearSolutionData()

  Call model.label("hydrocyclone.mph")

  Call model.get_component("comp1").get_view("view3").set("transparency", True)

  Call model.get_component("comp1").get_geom("geom1").get_run("rev1")
  Call model.get_component("comp1").get_geom("geom1").get_run("cyl1")
  Call model.get_component("comp1").get_geom("geom1").get_feature("cyl2").active(False)
  Call model.get_component("comp1").get_geom("geom1").get_run("fin")
  Call model.get_component("comp1").get_geom("geom1").get_run("cmd1")

  Call model.get_component("comp1").get_mesh("mesh1").run()

  Call model.get_sol("sol1").study("std1")

  Call model.get_study("std1").get_feature("stat").set("notsolnum", "auto")
  Call model.get_study("std1").get_feature("stat").set("notsolvertype", "solnum")

  Dim temp42(0 to 0) as String
  temp42(0) = "1"

  Call model.get_study("std1").get_feature("stat").set("notlistsolnum", temp42)
  Call model.get_study("std1").get_feature("stat").set("notsolnum", "auto")
  Call model.get_study("std1").get_feature("stat").set("notsolnumhide", "off")
  Call model.get_study("std1").get_feature("stat").set("notstudyhide", "off")
  Call model.get_study("std1").get_feature("stat").set("notsolhide", "off")
  Call model.get_study("std1").get_feature("stat").set("solnum", "auto")
  Call model.get_study("std1").get_feature("stat").set("solvertype", "solnum")

  Dim temp43(0 to 0) as String
  temp43(0) = "1"

  Call model.get_study("std1").get_feature("stat").set("listsolnum", temp43)
  Call model.get_study("std1").get_feature("stat").set("solnum", "auto")
  Call model.get_study("std1").get_feature("stat").set("solnumhide", "off")
  Call model.get_study("std1").get_feature("stat").set("initstudyhide", "off")
  Call model.get_study("std1").get_feature("stat").set("initsolhide", "off")

  Call model.get_sol("sol2").copySolution("sol3")

  Call model.result().get_dataset("dset2").set("solution", "none")

  Call model.get_study("std1").get_feature("wdi").set("notlistsolnum", 1)
  Call model.get_study("std1").get_feature("wdi").set("notsolnum", "auto")
  Call model.get_study("std1").get_feature("wdi").set("listsolnum", 1)
  Call model.get_study("std1").get_feature("wdi").set("solnum", "auto")
  Call model.get_study("std1").get_feature("stat").set("notlistsolnum", 1)
  Call model.get_study("std1").get_feature("stat").set("notsolnum", "auto")
  Call model.get_study("std1").get_feature("stat").set("listsolnum", 1)
  Call model.get_study("std1").get_feature("stat").set("solnum", "auto")

  Call model.get_sol("sol1").feature().remove("s2")
  Call model.get_sol("sol1").feature().remove("v2")
  Call model.get_sol("sol1").feature().remove("st2")
  Call model.get_sol("sol1").feature().remove("su1")
  Call model.get_sol("sol1").feature().remove("s1")
  Call model.get_sol("sol1").feature().remove("v1")
  Call model.get_sol("sol1").feature().remove("st1")
  Call model.get_sol("sol3").copySolution("sol2")

  Call model.sol().remove("sol3")

  Call model.get_sol("sol2").label("Solution Store 1")

  Call model.result().dataset().remove("dset4")

  Call model.get_sol("sol1").create("st1", "StudyStep")
  Call model.get_sol("sol1").get_feature("st1").set("study", "std1")
  Call model.get_sol("sol1").get_feature("st1").set("studystep", "wdi")
  Call model.get_sol("sol1").create("v1", "Variables")
  Call model.get_sol("sol1").get_feature("v1").set("control", "wdi")
  Call model.get_sol("sol1").create("s1", "Stationary")
  Call model.get_sol("sol1").get_feature("s1").set("stol", 1.0E-6)
  Call model.get_sol("sol1").get_feature("s1").create("fc1", "FullyCoupled")
  Call model.get_sol("sol1").get_feature("s1").get_feature("fc1").set("dtech", "auto")
  Call model.get_sol("sol1").get_feature("s1").get_feature("fc1").set("initstep", 0.01)
  Call model.get_sol("sol1").get_feature("s1").get_feature("fc1").set("minstep", 1.0E-6)
  Call model.get_sol("sol1").get_feature("s1").get_feature("fc1").set("maxiter", 50)
  Call model.get_sol("sol1").get_feature("s1").create("i1", "Iterative")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").set("linsolver", "gmres")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").set("prefuntype", "left")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").set("itrestart", 50)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").set("rhob", 20)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").set("maxlinit", 200)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").set("nlinnormuse", "on")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").label("Algebraic Multigrid Solver (spf)")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").create("mg1", "Multigrid")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").set("prefun", "saamg")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").set("mgcycle", "v")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").set("maxcoarsedof", 50000)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").set("strconn", 0.01)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").set("usesmooth", False)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").set("saamgcompwise", True)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("pr").create("sl1", "SORLine")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linesweeptype", "ssor")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linerelax", 0.7)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("pr").get_feature("sl1").set("iter", 0)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linealgorithm", "mesh")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linemethod", "uncoupled")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("pr").get_feature("sl1").set("seconditer", 1)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("pr").get_feature("sl1").set("relax", 0.5)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("po").create("sl1", "SORLine")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("po").get_feature("sl1").set("linesweeptype", "ssor")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("po").get_feature("sl1").set("linerelax", 0.7)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("po").get_feature("sl1").set("iter", 1)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("po").get_feature("sl1").set("linealgorithm", "mesh")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("po").get_feature("sl1").set("linemethod", "uncoupled")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("po").get_feature("sl1").set("seconditer", 1)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("po").get_feature("sl1").set("relax", 0.5)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("cs").create("d1", "Direct")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("cs").get_feature("d1").set("linsolver", "pardiso")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i1").get_feature("mg1").get_feature("cs").get_feature("d1").set("pivotperturb", 1.0E-13)
  Call model.get_sol("sol1").get_feature("s1").create("i2", "Iterative")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").set("linsolver", "gmres")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").set("prefuntype", "left")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").set("itrestart", 50)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").set("rhob", 20)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").set("maxlinit", 200)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").set("nlinnormuse", "on")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").label("Geometric Multigrid Solver (spf)")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").create("mg1", "Multigrid")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").set("prefun", "gmg")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").set("mcasegen", "any")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").set("gmglevels", 1)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("pr").create("sl1", "SORLine")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linesweeptype", "ssor")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linerelax", 0.7)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("pr").get_feature("sl1").set("iter", 0)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linealgorithm", "mesh")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linemethod", "uncoupled")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("pr").get_feature("sl1").set("seconditer", 1)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("pr").get_feature("sl1").set("relax", 0.5)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("po").create("sl1", "SORLine")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("po").get_feature("sl1").set("linesweeptype", "ssor")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("po").get_feature("sl1").set("linerelax", 0.7)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("po").get_feature("sl1").set("iter", 1)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("po").get_feature("sl1").set("linealgorithm", "mesh")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("po").get_feature("sl1").set("linemethod", "uncoupled")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("po").get_feature("sl1").set("seconditer", 1)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("po").get_feature("sl1").set("relax", 0.5)
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("cs").create("d1", "Direct")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("cs").get_feature("d1").set("linsolver", "pardiso")
  Call model.get_sol("sol1").get_feature("s1").get_feature("i2").get_feature("mg1").get_feature("cs").get_feature("d1").set("pivotperturb", 1.0E-13)
  Call model.get_sol("sol1").get_feature("s1").get_feature("fc1").set("linsolver", "i1")
  Call model.get_sol("sol1").get_feature("s1").get_feature("fc1").set("dtech", "auto")
  Call model.get_sol("sol1").get_feature("s1").get_feature("fc1").set("initstep", 0.01)
  Call model.get_sol("sol1").get_feature("s1").get_feature("fc1").set("minstep", 1.0E-6)
  Call model.get_sol("sol1").get_feature("s1").get_feature("fc1").set("maxiter", 50)
  Call model.get_sol("sol1").get_feature("s1").feature().remove("fcDef")
  Call model.get_sol("sol1").create("su1", "StoreSolution")
  Call model.get_sol("sol1").get_feature("su1").set("sol", "sol2")
  Call model.get_sol("sol1").get_feature("su1").label("Solution Store 1")
  Call model.get_sol("sol1").create("st2", "StudyStep")
  Call model.get_sol("sol1").get_feature("st2").set("study", "std1")
  Call model.get_sol("sol1").get_feature("st2").set("studystep", "stat")

  Call model.get_study("std1").get_feature("stat").set("notsoluse", "sol2")

  Call model.get_sol("sol1").create("v2", "Variables")
  Call model.get_sol("sol1").get_feature("v2").set("initmethod", "sol")
  Call model.get_sol("sol1").get_feature("v2").set("initsol", "sol1")
  Call model.get_sol("sol1").get_feature("v2").set("notsolmethod", "sol")
  Call model.get_sol("sol1").get_feature("v2").set("notsol", "sol1")
  Call model.get_sol("sol1").get_feature("v2").set("control", "stat")
  Call model.get_sol("sol1").create("s2", "Stationary")
  Call model.get_sol("sol1").get_feature("s2").get_feature("aDef").set("cachepattern", True)
  Call model.get_sol("sol1").get_feature("s2").create("se1", "Segregated")
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").feature().remove("ssDef")
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").create("ss1", "SegregatedStep")

  Dim temp44(0 to 1) as String
  temp44(0) = "comp1_u"
  temp44(1) = "comp1_p"

  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").get_feature("ss1").set("segvar", temp44)
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").get_feature("ss1").set("subdamp", 0.5)
  Call model.get_sol("sol1").get_feature("s2").create("i1", "Iterative")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").set("linsolver", "gmres")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").set("prefuntype", "left")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").set("itrestart", 50)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").set("rhob", 20)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").set("maxlinit", 200)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").set("nlinnormuse", "on")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").label("Algebraic Multigrid Solver (spf)")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").create("mg1", "Multigrid")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").set("prefun", "saamg")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").set("mgcycle", "f")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").set("maxcoarsedof", 80000)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").set("strconn", 0.02)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").set("usesmooth", False)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").set("saamgcompwise", True)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").get_feature("pr").create("sc1", "SCGS")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").get_feature("pr").get_feature("sc1").set("linesweeptype", "ssor")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").get_feature("pr").get_feature("sc1").set("iter", 0)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").get_feature("pr").get_feature("sc1").set("scgsrelax", 0.7)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").get_feature("pr").get_feature("sc1").set("scgsmethod", "lines_vertices")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").get_feature("pr").get_feature("sc1").set("scgsvertexrelax", 0.7)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").get_feature("pr").get_feature("sc1").set("seconditer", 1)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").get_feature("pr").get_feature("sc1").set("relax", 0.5)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").get_feature("po").create("sc1", "SCGS")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").get_feature("po").get_feature("sc1").set("linesweeptype", "ssor")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").get_feature("po").get_feature("sc1").set("iter", 1)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").get_feature("po").get_feature("sc1").set("scgsrelax", 0.7)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").get_feature("po").get_feature("sc1").set("scgsmethod", "lines_vertices")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").get_feature("po").get_feature("sc1").set("scgsvertexrelax", 0.7)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").get_feature("po").get_feature("sc1").set("seconditer", 1)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").get_feature("po").get_feature("sc1").set("relax", 0.5)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").get_feature("cs").create("d1", "Direct")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").get_feature("cs").get_feature("d1").set("linsolver", "pardiso")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i1").get_feature("mg1").get_feature("cs").get_feature("d1").set("pivotperturb", 1.0E-13)
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").get_feature("ss1").set("linsolver", "i1")
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").get_feature("ss1").label("Velocity u, Pressure p")
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").create("ss2", "SegregatedStep")

  Dim temp45(0 to 2) as String
  temp45(0) = "comp1_k"
  temp45(1) = "comp1_ep"
  temp45(2) = "comp1_zeta"

  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").get_feature("ss2").set("segvar", temp45)
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").get_feature("ss2").set("subdamp", 0.35)
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").get_feature("ss2").set("subiter", 3)
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").get_feature("ss2").set("subtermconst", "itertol")
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").get_feature("ss2").set("subntolfact", 1)
  Call model.get_sol("sol1").get_feature("s2").create("i2", "Iterative")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").set("linsolver", "gmres")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").set("prefuntype", "left")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").set("itrestart", 50)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").set("rhob", 20)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").set("maxlinit", 200)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").set("nlinnormuse", "on")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").label("Algebraic Multigrid Solver")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").create("mg1", "Multigrid")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").set("prefun", "saamg")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").set("mgcycle", "v")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").set("maxcoarsedof", 50000)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").set("strconn", 0.01)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").set("usesmooth", False)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").set("saamgcompwise", True)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").get_feature("pr").create("sl1", "SORLine")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linesweeptype", "ssor")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linerelax", 0.7)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").get_feature("pr").get_feature("sl1").set("iter", 0)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linealgorithm", "mesh")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linemethod", "uncoupled")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").get_feature("pr").get_feature("sl1").set("seconditer", 1)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").get_feature("pr").get_feature("sl1").set("relax", 0.5)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").get_feature("po").create("sl1", "SORLine")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").get_feature("po").get_feature("sl1").set("linesweeptype", "ssor")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").get_feature("po").get_feature("sl1").set("linerelax", 0.7)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").get_feature("po").get_feature("sl1").set("iter", 1)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").get_feature("po").get_feature("sl1").set("linealgorithm", "mesh")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").get_feature("po").get_feature("sl1").set("linemethod", "uncoupled")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").get_feature("po").get_feature("sl1").set("seconditer", 1)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").get_feature("po").get_feature("sl1").set("relax", 0.5)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").get_feature("cs").create("d1", "Direct")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").get_feature("cs").get_feature("d1").set("linsolver", "pardiso")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i2").get_feature("mg1").get_feature("cs").get_feature("d1").set("pivotperturb", 1.0E-13)
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").get_feature("ss2").set("linsolver", "i2")
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").get_feature("ss2").label("Turbulence variables")
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").create("ss3", "SegregatedStep")

  Dim temp46(0 to 0) as String
  temp46(0) = "comp1_alpha"

  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").get_feature("ss3").set("segvar", temp46)
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").get_feature("ss3").set("subdamp", 1)
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").get_feature("ss3").set("subtermconst", "iter")
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").get_feature("ss3").set("subiter", 1)
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").get_feature("ss3").set("subntolfact", 1)
  Call model.get_sol("sol1").get_feature("s2").create("i3", "Iterative")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").set("linsolver", "gmres")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").set("prefuntype", "left")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").set("itrestart", 50)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").set("rhob", 20)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").set("maxlinit", 200)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").set("nlinnormuse", "on")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").label("Algebraic Multigrid Solver 2")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").create("mg1", "Multigrid")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").set("prefun", "saamg")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").set("mgcycle", "v")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").set("maxcoarsedof", 50000)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").set("strconn", 0.01)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").set("usesmooth", False)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").set("saamgcompwise", True)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").get_feature("pr").create("sl1", "SORLine")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linesweeptype", "ssor")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linerelax", 0.7)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").get_feature("pr").get_feature("sl1").set("iter", 0)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linealgorithm", "mesh")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linemethod", "uncoupled")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").get_feature("pr").get_feature("sl1").set("seconditer", 1)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").get_feature("pr").get_feature("sl1").set("relax", 0.5)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").get_feature("po").create("sl1", "SORLine")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").get_feature("po").get_feature("sl1").set("linesweeptype", "ssor")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").get_feature("po").get_feature("sl1").set("linerelax", 0.7)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").get_feature("po").get_feature("sl1").set("iter", 1)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").get_feature("po").get_feature("sl1").set("linealgorithm", "mesh")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").get_feature("po").get_feature("sl1").set("linemethod", "uncoupled")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").get_feature("po").get_feature("sl1").set("seconditer", 1)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").get_feature("po").get_feature("sl1").set("relax", 0.5)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").get_feature("cs").create("d1", "Direct")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").get_feature("cs").get_feature("d1").set("linsolver", "pardiso")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i3").get_feature("mg1").get_feature("cs").get_feature("d1").set("pivotperturb", 1.0E-13)
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").get_feature("ss3").set("linsolver", "i3")
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").get_feature("ss3").label("Elliptic blending function")
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").set("segstabacc", "segcflcmp")
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").set("subinitcfl", 2)
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").set("subkppid", 0.65)
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").set("subkdpid", 0.03)
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").set("subkipid", 0.05)
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").set("subcfltol", 0.08)
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").set("maxsegiter", 400)
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").create("ll1", "LowerLimit")
  Call model.get_sol("sol1").get_feature("s2").get_feature("se1").get_feature("ll1").set("lowerlimit", "comp1.k 0 comp1.ep 0 comp1.zeta 0 comp1.alpha 0 ")
  Call model.get_sol("sol1").get_feature("s2").create("i4", "Iterative")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").set("linsolver", "gmres")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").set("prefuntype", "left")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").set("itrestart", 50)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").set("rhob", 20)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").set("maxlinit", 200)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").set("nlinnormuse", "on")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").label("Geometric Multigrid Solver (spf)")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").create("mg1", "Multigrid")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").get_feature("mg1").set("prefun", "gmg")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").get_feature("mg1").set("mcasegen", "any")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").get_feature("mg1").set("gmglevels", 2)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").get_feature("mg1").get_feature("pr").create("sc1", "SCGS")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").get_feature("mg1").get_feature("pr").get_feature("sc1").set("linesweeptype", "ssor")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").get_feature("mg1").get_feature("pr").get_feature("sc1").set("iter", 0)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").get_feature("mg1").get_feature("pr").get_feature("sc1").set("scgsrelax", 0.7)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").get_feature("mg1").get_feature("pr").get_feature("sc1").set("scgsmethod", "lines_vertices")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").get_feature("mg1").get_feature("pr").get_feature("sc1").set("scgsvertexrelax", 0.7)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").get_feature("mg1").get_feature("pr").get_feature("sc1").set("seconditer", 1)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").get_feature("mg1").get_feature("pr").get_feature("sc1").set("relax", 0.5)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").get_feature("mg1").get_feature("po").create("sc1", "SCGS")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").get_feature("mg1").get_feature("po").get_feature("sc1").set("linesweeptype", "ssor")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").get_feature("mg1").get_feature("po").get_feature("sc1").set("iter", 1)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").get_feature("mg1").get_feature("po").get_feature("sc1").set("scgsrelax", 0.7)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").get_feature("mg1").get_feature("po").get_feature("sc1").set("scgsmethod", "lines_vertices")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").get_feature("mg1").get_feature("po").get_feature("sc1").set("scgsvertexrelax", 0.7)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").get_feature("mg1").get_feature("po").get_feature("sc1").set("seconditer", 1)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").get_feature("mg1").get_feature("po").get_feature("sc1").set("relax", 0.5)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").get_feature("mg1").get_feature("cs").create("d1", "Direct")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").get_feature("mg1").get_feature("cs").get_feature("d1").set("linsolver", "pardiso")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i4").get_feature("mg1").get_feature("cs").get_feature("d1").set("pivotperturb", 1.0E-13)
  Call model.get_sol("sol1").get_feature("s2").create("i5", "Iterative")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").set("linsolver", "gmres")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").set("prefuntype", "left")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").set("itrestart", 50)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").set("rhob", 20)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").set("maxlinit", 200)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").set("nlinnormuse", "on")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").label("Geometric Multigrid Solver")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").create("mg1", "Multigrid")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").get_feature("mg1").set("prefun", "gmg")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").get_feature("mg1").set("mcasegen", "any")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").get_feature("mg1").set("gmglevels", 1)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").get_feature("mg1").get_feature("pr").create("sl1", "SORLine")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linesweeptype", "ssor")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linerelax", 0.7)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").get_feature("mg1").get_feature("pr").get_feature("sl1").set("iter", 0)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linealgorithm", "mesh")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linemethod", "uncoupled")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").get_feature("mg1").get_feature("pr").get_feature("sl1").set("seconditer", 1)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").get_feature("mg1").get_feature("pr").get_feature("sl1").set("relax", 0.5)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").get_feature("mg1").get_feature("po").create("sl1", "SORLine")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").get_feature("mg1").get_feature("po").get_feature("sl1").set("linesweeptype", "ssor")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").get_feature("mg1").get_feature("po").get_feature("sl1").set("linerelax", 0.7)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").get_feature("mg1").get_feature("po").get_feature("sl1").set("iter", 1)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").get_feature("mg1").get_feature("po").get_feature("sl1").set("linealgorithm", "mesh")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").get_feature("mg1").get_feature("po").get_feature("sl1").set("linemethod", "uncoupled")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").get_feature("mg1").get_feature("po").get_feature("sl1").set("seconditer", 1)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").get_feature("mg1").get_feature("po").get_feature("sl1").set("relax", 0.5)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").get_feature("mg1").get_feature("cs").create("d1", "Direct")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").get_feature("mg1").get_feature("cs").get_feature("d1").set("linsolver", "pardiso")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i5").get_feature("mg1").get_feature("cs").get_feature("d1").set("pivotperturb", 1.0E-13)
  Call model.get_sol("sol1").get_feature("s2").create("i6", "Iterative")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").set("linsolver", "gmres")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").set("prefuntype", "left")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").set("itrestart", 50)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").set("rhob", 20)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").set("maxlinit", 200)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").set("nlinnormuse", "on")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").label("Geometric Multigrid Solver 2")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").create("mg1", "Multigrid")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").get_feature("mg1").set("prefun", "gmg")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").get_feature("mg1").set("mcasegen", "any")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").get_feature("mg1").set("gmglevels", 1)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").get_feature("mg1").get_feature("pr").create("sl1", "SORLine")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linesweeptype", "ssor")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linerelax", 0.7)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").get_feature("mg1").get_feature("pr").get_feature("sl1").set("iter", 0)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linealgorithm", "mesh")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").get_feature("mg1").get_feature("pr").get_feature("sl1").set("linemethod", "uncoupled")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").get_feature("mg1").get_feature("pr").get_feature("sl1").set("seconditer", 1)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").get_feature("mg1").get_feature("pr").get_feature("sl1").set("relax", 0.5)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").get_feature("mg1").get_feature("po").create("sl1", "SORLine")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").get_feature("mg1").get_feature("po").get_feature("sl1").set("linesweeptype", "ssor")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").get_feature("mg1").get_feature("po").get_feature("sl1").set("linerelax", 0.7)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").get_feature("mg1").get_feature("po").get_feature("sl1").set("iter", 1)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").get_feature("mg1").get_feature("po").get_feature("sl1").set("linealgorithm", "mesh")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").get_feature("mg1").get_feature("po").get_feature("sl1").set("linemethod", "uncoupled")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").get_feature("mg1").get_feature("po").get_feature("sl1").set("seconditer", 1)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").get_feature("mg1").get_feature("po").get_feature("sl1").set("relax", 0.5)
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").get_feature("mg1").get_feature("cs").create("d1", "Direct")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").get_feature("mg1").get_feature("cs").get_feature("d1").set("linsolver", "pardiso")
  Call model.get_sol("sol1").get_feature("s2").get_feature("i6").get_feature("mg1").get_feature("cs").get_feature("d1").set("pivotperturb", 1.0E-13)
  Call model.get_sol("sol1").get_feature("s2").feature().remove("fcDef")

  Call model.result().get_dataset("dset2").set("solution", "sol2")

  Call model.get_sol("sol1").get_feature("v2").set("notsolnum", "auto")
  Call model.get_sol("sol1").get_feature("v2").set("notsolvertype", "solnum")

  Dim temp47(0 to 0) as String
  temp47(0) = "1"

  Call model.get_sol("sol1").get_feature("v2").set("notlistsolnum", temp47)
  Call model.get_sol("sol1").get_feature("v2").set("notsolnum", "auto")

  Dim temp48(0 to 0) as String
  temp48(0) = "1"

  Call model.get_sol("sol1").get_feature("v2").set("notlistsolnum", temp48)
  Call model.get_sol("sol1").get_feature("v2").set("notsolnum", "auto")
  Call model.get_sol("sol1").get_feature("v2").set("control", "stat")
  Call model.get_sol("sol1").get_feature("v2").set("solnum", "auto")
  Call model.get_sol("sol1").get_feature("v2").set("solvertype", "solnum")

  Dim temp49(0 to 0) as String
  temp49(0) = "1"

  Call model.get_sol("sol1").get_feature("v2").set("listsolnum", temp49)
  Call model.get_sol("sol1").get_feature("v2").set("solnum", "auto")

  Dim temp50(0 to 0) as String
  temp50(0) = "1"

  Call model.get_sol("sol1").get_feature("v2").set("listsolnum", temp50)
  Call model.get_sol("sol1").get_feature("v2").set("solnum", "auto")

  Call model.get_study("std1").get_feature("stat").set("solnum", "auto")
  Call model.get_study("std1").get_feature("stat").set("solvertype", "solnum")

  Dim temp51(0 to 0) as String
  temp51(0) = "1"

  Call model.get_study("std1").get_feature("stat").set("listsolnum", temp51)
  Call model.get_study("std1").get_feature("stat").set("solnum", "auto")

  Dim temp52(0 to 0) as String
  temp52(0) = "1"

  Call model.get_study("std1").get_feature("stat").set("listsolnum", temp52)
  Call model.get_study("std1").get_feature("stat").set("solnum", "auto")
  Call model.get_study("std1").get_feature("stat").set("solnumhide", "off")
  Call model.get_study("std1").get_feature("stat").set("initstudyhide", "off")
  Call model.get_study("std1").get_feature("stat").set("initsolhide", "off")

  Call model.get_sol("sol1").attach("std1")
  Call model.get_sol("sol1").runAll()

  Call model.get_result("pg1").run()
  Call model.get_result("pg2").run()
  Call model.get_result("pg3").run()
  Call model.get_result("pg4").run()
  Call model.get_result("pg5").run()
  Call model.get_result("pg1").run()
  Call model.get_result("pg1").run()
  Call model.get_result("pg1").run()
  Call model.get_result("pg1").run()
  Call model.get_result("pg1").run()
  Call model.get_result("pg1").get_feature("line1").active(False)
  Call model.get_result("pg1").run()
  Call model.get_result("pg1").get_feature("line1").active(True)
  Call model.get_result("pg1").run()
  Call model.get_result("pg1").run()
  Call model.get_result("pg1").get_feature("surf1").active(False)
  Call model.get_result("pg1").run()
  Call model.get_result("pg1").run()
  Call model.get_result("pg1").get_feature("line1").active(False)
  Call model.get_result("pg1").run()
  Call model.get_result("pg1").run()
  Call model.get_result("pg1").run()
  Call model.get_result("pg1").run()
  Call model.get_result("pg1").run()
  Call model.get_result("pg1").run()
  Call model.get_result("pg1").get_feature("str1").set("linetype", "line")
  Call model.get_result("pg1").run()

  Dim temp53(0 to 4) as Long
  temp53(0) = 13
  temp53(1) = 14
  temp53(2) = 23
  temp53(3) = 26
  temp53(4) = 35

  Call model.get_result("pg1").get_feature("str1").selection().set(temp53)
  Call model.get_result("pg1").run()
  Call model.get_result("pg1").run()
  Call model.get_result("pg1").get_feature("str2").active(False)
  Call model.get_result("pg1").run()
  Call model.get_result("pg1").run()
  Call model.get_result("pg1").get_feature("str1").active(False)
  Call model.get_result("pg1").run()
  Call model.get_result("pg1").run()
  Call model.get_result("pg1").get_feature("str2").active(True)
  Call model.get_result("pg1").run()

  Dim temp54(0 to 4) as Long
  temp54(0) = 15
  temp54(1) = 16
  temp54(2) = 24
  temp54(3) = 25
  temp54(4) = 35

  Call model.get_result("pg1").get_feature("str2").selection().set(temp54)
  Call model.get_result("pg1").run()
  Call model.get_result("pg1").get_feature("str2").set("linetype", "line")
  Call model.get_result("pg1").get_feature("str2").set("selnumber", 1)
  Call model.get_result("pg1").run()
  Call model.get_result("pg1").get_feature("str2").set("selnumber", 5)
  Call model.get_result("pg1").run()
  Call model.get_result("pg1").run()
  Call model.get_result("pg1").get_feature("surf1").active(True)
  Call model.get_result("pg1").run()
  Call model.get_result("pg1").run()
  Call model.get_result("pg1").run()
  Call model.get_result("pg1").get_feature("str2").set("selnumber", 10)
  Call model.get_result("pg1").run()
  Call model.get_result("pg1").run()
  Call model.get_result("pg1").get_feature("str1").active(True)
  Call model.get_result("pg1").run()
  Call model.get_result("pg1").run()
  Call model.get_result("pg1").get_feature("surf1").active(False)
  Call model.get_result("pg1").run()
  Call model.get_result("pg1").set("allowtableupdate", True)

  Call model.label("hydrocyclone.mph")

  Call model.get_result("pg1").run()

  Call model.get_sol("sol1").clearSolutionData()
  Call model.get_sol("sol2").clearSolutionData()

  Call model.get_result("pg1").run()
  Call model.get_result("pg1").run()
  Call model.get_result("pg2").run()
  Call model.get_result("pg4").run()
  Call model.get_result("pg3").run()
  Call model.get_result("pg5").run()

End Sub
